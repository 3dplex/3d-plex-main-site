<?php
namespace app;

$library = __DIR__ . '/vendor/Drewm/MailChimp.php';
file_exists($library) ? require $library : die(json_encode(array(
	'status' => 'MailChimp library cannot be found!'
)));

use Drewm\MailChimp;
header('Content-Type: application/json');

if (isset($_POST['email'])) {
	$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$MailChimp = new MailChimp('391c3643a4f66be6fdb1677e798e34b9-us11');
		$result = $MailChimp->call('lists/subscribe', array(
			'id' => '132e495e62',
			'email' => array('email' => $_POST['email']),
			'double_optin' => false,
			'update_existing' => true,
			'replace_interests' => false,
			'send_welcome' => false,
		));
		die(json_encode($result));
	}
}

die(json_encode(array(
	'status' => 'error',
	'code' => 500,
	'name' => 'Invalid_Email',
	'error' => 'The email you are trying to subscribe is denied by mailChimp.'
)));
