<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/landing/user/config/site.yaml',
    'modified' => 1458377473,
    'data' => [
        'title' => '3D Plex',
        'author' => [
            'name' => '3dplex',
            'email' => '3dplex.io@gmail.com'
        ],
        'taxonomies' => [
            0 => 'category',
            1 => 'tag'
        ],
        'metadata' => [
            'viewport' => 'width=device-width, initial-scale=1',
            'apple-mobile-web-app-capable' => 'yes',
            'description' => '3DPlex builds an IoT platform that introduces Apps for 3D Printers.',
            'keywords' => 'IoT, internet of things, 3d printing, framework, open source, smart devices, apps, 3d printing services',
            'author' => '3D Plex'
        ],
        'summary' => [
            'enabled' => true,
            'format' => 'short',
            'size' => 300,
            'delimiter' => '==='
        ],
        'blog' => [
            'route' => '/blog'
        ]
    ]
];
