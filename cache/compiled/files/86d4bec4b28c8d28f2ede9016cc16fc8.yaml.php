<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'D:/xampp/htdocs/grav-admin/user/themes/plex/blueprints.yaml',
    'modified' => 1458375906,
    'data' => [
        'name' => '3D Plex',
        'version' => '1.0.0',
        'description' => 'Default theme for the **3D Plex** site.',
        'icon' => 'empire',
        'author' => [
            'name' => 'Panagiotis Papadatis',
            'email' => 'ppapadatis@protonmail.com',
            'url' => 'https://github.com/hennett'
        ],
        'homepage' => '',
        'demo' => '',
        'keywords' => '3dplex, theme, core, modern, fast, responsive, html5, css3',
        'bugs' => '',
        'license' => 'MIT'
    ]
];
