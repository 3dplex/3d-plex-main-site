<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/landing/user/accounts/3dplex.yaml',
    'modified' => 1458344296,
    'data' => [
        'email' => '3dplex.io@gmail.com',
        'fullname' => '3dplex',
        'title' => 'admin',
        'state' => 'enabled',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$udHKJLbh9TqXirJ4Kk4k/.tBacRhsbzjyTYHnYeXnj3uAQrDQNtdq'
    ]
];
