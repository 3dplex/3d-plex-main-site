<?php

/* partials/base.html.twig */
class __TwigTemplate_0b03eb460748fb1eb62bfcc1a33b98faed534cf1d9e33b1fcf2888acf36fb53f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'preloader' => array($this, 'block_preloader'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'bottom' => array($this, 'block_bottom'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
\t<head>
\t\t";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 33
        echo "\t</head>
\t<body>
\t\t";
        // line 35
        $this->displayBlock('preloader', $context, $blocks);
        // line 38
        echo "
\t\t";
        // line 39
        $this->displayBlock('body', $context, $blocks);
        // line 42
        echo "
\t\t";
        // line 43
        $this->displayBlock('footer', $context, $blocks);
        // line 46
        echo "
\t\t";
        // line 47
        $this->displayBlock('bottom', $context, $blocks);
        // line 62
        echo "\t\t";
        echo $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "js", array(0 => "bottom"), "method");
        echo "
\t\t<script type=\"text/javascript\">
    \t\twindow.cookieconsent_options = {\"message\":\"This website uses cookies to ensure you get the best experience on our website\",\"dismiss\":\"Got it!\",\"theme\":\"dark-bottom\"};
\t\t</script>
\t</body>
</html>
";
    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        // line 5
        echo "\t\t\t<meta charset=\"utf-8\">
\t\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t\t";
        // line 7
        $this->loadTemplate("partials/metadata.html.twig", "partials/base.html.twig", 7)->display($context);
        // line 8
        echo "
\t\t\t<title>";
        // line 9
        echo $this->getAttribute((isset($context["site"]) ? $context["site"] : null), "title", array());
        echo "</title>
\t\t\t<link rel=\"shortcut icon\" href=\"";
        // line 10
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://img/favicon.ico");
        echo "\" />

\t\t\t";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "\t\t\t";
        echo $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "css", array(), "method");
        echo "

\t\t\t";
        // line 24
        $this->displayBlock('javascripts', $context, $blocks);
        // line 32
        echo "\t\t";
    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 13
        echo "\t\t        ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css/bootstrap.min.css"), "method");
        // line 14
        echo "\t\t        ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css/animate.min.css"), "method");
        // line 15
        echo "\t\t        ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css/font-awesome.min.css"), "method");
        // line 16
        echo "\t\t        ";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css/morphext.css"), "method");
        // line 17
        echo "\t\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css/toastr.min.css"), "method");
        // line 18
        echo "\t\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css/global.css"), "method");
        // line 19
        echo "\t\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "theme://css/breakpoints.css"), "method");
        // line 20
        echo "\t\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addCss", array(0 => "http://fonts.googleapis.com/css?family=Open+Sans:400,300,600italic,600,700"), "method");
        // line 21
        echo "\t\t\t";
    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
        // line 25
        echo "\t\t        ";
        if (((($this->getAttribute((isset($context["browser"]) ? $context["browser"] : null), "getBrowser", array()) == "msie") && ($this->getAttribute((isset($context["browser"]) ? $context["browser"] : null), "getVersion", array()) >= 8)) && ($this->getAttribute((isset($context["browser"]) ? $context["browser"] : null), "getVersion", array()) <= 9))) {
            // line 26
            echo "\t\t            ";
            $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js", 1 => array("group" => "javascripts")), "method");
            // line 27
            echo "\t\t            ";
            $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "https://oss.maxcdn.com/respond/1.4.2/respond.min.js", 1 => array("group" => "javascripts")), "method");
            // line 28
            echo "\t\t        ";
        }
        // line 29
        echo "\t\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/modernizr.custom.js", 1 => array("group" => "javascripts")), "method");
        // line 30
        echo "\t\t        ";
        echo $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "js", array(0 => "javascripts"), "method");
        echo "
\t\t    ";
    }

    // line 35
    public function block_preloader($context, array $blocks = array())
    {
        // line 36
        echo "\t\t\t";
        $this->loadTemplate("partials/preloader.html.twig", "partials/base.html.twig", 36)->display($context);
        // line 37
        echo "\t\t";
    }

    // line 39
    public function block_body($context, array $blocks = array())
    {
        // line 40
        echo "\t\t\t";
        $this->displayBlock('content', $context, $blocks);
        // line 41
        echo "\t\t";
    }

    // line 40
    public function block_content($context, array $blocks = array())
    {
    }

    // line 43
    public function block_footer($context, array $blocks = array())
    {
        // line 44
        echo "\t\t\t";
        $this->loadTemplate("partials/footer.html.twig", "partials/base.html.twig", 44)->display($context);
        // line 45
        echo "\t\t";
    }

    // line 47
    public function block_bottom($context, array $blocks = array())
    {
        // line 48
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/jquery-1.12.0.min.js", 1 => array("group" => "bottom")), "method");
        // line 49
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/bootstrap.min.js", 1 => array("group" => "bottom")), "method");
        // line 50
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/jquery.parallax-1.1.3.js", 1 => array("group" => "bottom")), "method");
        // line 51
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/smooth-scroll.min.js", 1 => array("group" => "bottom")), "method");
        // line 52
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/morphext.min.js", 1 => array("group" => "bottom")), "method");
        // line 53
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/jquery.vide.min.js", 1 => array("group" => "bottom")), "method");
        // line 54
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/wow.min.js", 1 => array("group" => "bottom")), "method");
        // line 55
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/toastr.min.js", 1 => array("group" => "bottom")), "method");
        // line 56
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/is.min.js", 1 => array("group" => "bottom")), "method");
        // line 57
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/canvas.js", 1 => array("group" => "bottom")), "method");
        // line 58
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/preloader.js", 1 => array("group" => "bottom")), "method");
        // line 59
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "theme://js/global.js", 1 => array("group" => "bottom")), "method");
        // line 60
        echo "\t\t\t";
        $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "addJs", array(0 => "//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js", 1 => array("group" => "bottom")), "method");
        // line 61
        echo "\t\t";
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 61,  232 => 60,  229 => 59,  226 => 58,  223 => 57,  220 => 56,  217 => 55,  214 => 54,  211 => 53,  208 => 52,  205 => 51,  202 => 50,  199 => 49,  196 => 48,  193 => 47,  189 => 45,  186 => 44,  183 => 43,  178 => 40,  174 => 41,  171 => 40,  168 => 39,  164 => 37,  161 => 36,  158 => 35,  151 => 30,  148 => 29,  145 => 28,  142 => 27,  139 => 26,  136 => 25,  133 => 24,  129 => 21,  126 => 20,  123 => 19,  120 => 18,  117 => 17,  114 => 16,  111 => 15,  108 => 14,  105 => 13,  102 => 12,  98 => 32,  96 => 24,  90 => 22,  88 => 12,  83 => 10,  79 => 9,  76 => 8,  74 => 7,  70 => 5,  67 => 4,  55 => 62,  53 => 47,  50 => 46,  48 => 43,  45 => 42,  43 => 39,  40 => 38,  38 => 35,  34 => 33,  32 => 4,  27 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
\t<head>
\t\t{% block head %}
\t\t\t<meta charset=\"utf-8\">
\t\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t\t{% include 'partials/metadata.html.twig' %}

\t\t\t<title>{{ site.title }}</title>
\t\t\t<link rel=\"shortcut icon\" href=\"{{ url('theme://img/favicon.ico') }}\" />

\t\t\t{% block stylesheets %}
\t\t        {% do assets.addCss('theme://css/bootstrap.min.css') %}
\t\t        {% do assets.addCss('theme://css/animate.min.css') %}
\t\t        {% do assets.addCss('theme://css/font-awesome.min.css') %}
\t\t        {% do assets.addCss('theme://css/morphext.css') %}
\t\t\t\t{% do assets.addCss('theme://css/toastr.min.css') %}
\t\t\t\t{% do assets.addCss('theme://css/global.css') %}
\t\t\t\t{% do assets.addCss('theme://css/breakpoints.css') %}
\t\t\t\t{% do assets.addCss('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600italic,600,700') %}
\t\t\t{% endblock %}
\t\t\t{{ assets.css() }}

\t\t\t{% block javascripts %}
\t\t        {% if browser.getBrowser == 'msie' and browser.getVersion >= 8 and browser.getVersion <= 9 %}
\t\t            {% do assets.addJs('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js', {group: 'javascripts'}) %}
\t\t            {% do assets.addJs('https://oss.maxcdn.com/respond/1.4.2/respond.min.js', {group: 'javascripts'}) %}
\t\t        {% endif %}
\t\t\t\t{% do assets.addJs('theme://js/modernizr.custom.js', {group: 'javascripts'}) %}
\t\t        {{ assets.js('javascripts') }}
\t\t    {% endblock %}
\t\t{% endblock head %}
\t</head>
\t<body>
\t\t{% block preloader %}
\t\t\t{% include 'partials/preloader.html.twig' %}
\t\t{% endblock %}

\t\t{% block body %}
\t\t\t{% block content %}{% endblock %}
\t\t{% endblock %}

\t\t{% block footer %}
\t\t\t{% include 'partials/footer.html.twig' %}
\t\t{% endblock %}

\t\t{% block bottom %}
\t\t\t{% do assets.addJs('theme://js/jquery-1.12.0.min.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/bootstrap.min.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/jquery.parallax-1.1.3.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/smooth-scroll.min.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/morphext.min.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/jquery.vide.min.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/wow.min.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/toastr.min.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/is.min.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/canvas.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/preloader.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('theme://js/global.js', {group: 'bottom'}) %}
\t\t\t{% do assets.addJs('//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js', {group: 'bottom'}) %}
\t\t{% endblock %}
\t\t{{ assets.js('bottom') }}
\t\t<script type=\"text/javascript\">
    \t\twindow.cookieconsent_options = {\"message\":\"This website uses cookies to ensure you get the best experience on our website\",\"dismiss\":\"Got it!\",\"theme\":\"dark-bottom\"};
\t\t</script>
\t</body>
</html>
", "partials/base.html.twig", "/var/www/html/landing/user/themes/plex/templates/partials/base.html.twig");
    }
}
