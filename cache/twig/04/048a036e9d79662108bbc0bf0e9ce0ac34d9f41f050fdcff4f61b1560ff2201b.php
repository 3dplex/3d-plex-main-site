<?php

/* modular/promo-one.html.twig */
class __TwigTemplate_264f7738018d9ca170162055867034d06ce182c7762e2ce4111fb5429285f464 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"promo-one\" class=\"parallax-section\">
    <div class=\"parallax-content\">
        <div class=\"color-overlay\"></div>
        <div class=\"container text-center\">
            <h2>";
        // line 5
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "title", array());
        echo "</h2>
            ";
        // line 6
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "modular/promo-one.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 6,  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"promo-one\" class=\"parallax-section\">
    <div class=\"parallax-content\">
        <div class=\"color-overlay\"></div>
        <div class=\"container text-center\">
            <h2>{{ page.header.title }}</h2>
            {{ page.content }}
        </div>
    </div>
</div>
", "modular/promo-one.html.twig", "/var/www/html/landing/user/themes/plex/templates/modular/promo-one.html.twig");
    }
}
