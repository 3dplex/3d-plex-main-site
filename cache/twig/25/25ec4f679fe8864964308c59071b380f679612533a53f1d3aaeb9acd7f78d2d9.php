<?php

/* partials/footer.html.twig */
class __TwigTemplate_e69b5c1122128a450d980c0f86abcf50db43301870a95e1d33728ffb97315a55 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer id=\"footer\">
    <div class=\"container text-center\">
        <p>&copy; ";
        // line 3
        echo twig_date_format_filter($this->env, "now", "Y");
        echo " <a href=\"";
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "\">3dplex.io</a>. All rights reserved.</p>
    </div>
    <a data-scroll href=\"#home\" class=\"to-top\"><i class=\"fa fa-angle-up\"></i></a>
</footer>
";
    }

    public function getTemplateName()
    {
        return "partials/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer id=\"footer\">
    <div class=\"container text-center\">
        <p>&copy; {{ \"now\"|date('Y') }} <a href=\"{{ base_url }}\">3dplex.io</a>. All rights reserved.</p>
    </div>
    <a data-scroll href=\"#home\" class=\"to-top\"><i class=\"fa fa-angle-up\"></i></a>
</footer>
", "partials/footer.html.twig", "/var/www/html/landing/user/themes/plex/templates/partials/footer.html.twig");
    }
}
