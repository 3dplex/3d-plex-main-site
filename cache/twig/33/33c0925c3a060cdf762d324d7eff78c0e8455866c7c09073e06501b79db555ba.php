<?php

/* partials/navigation.html.twig */
class __TwigTemplate_7a045d0c9239601dbdb78aaaaac20f17102f26eca879d3bd108e5823a621b147 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"navigation\">
\t<div class=\"navbar\" role=\"banner\">
\t\t<div class=\"container\">
\t\t\t<div class=\"navbar-header\">
\t\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t</button>
\t\t\t\t<a class=\"navbar-brand\" href=\"";
        // line 11
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "\"><h1><img class=\"img-responsive\" src=\"";
        echo $this->env->getExtension('GravTwigExtension')->urlFunc("theme://img/logo_black_small.png");
        echo "\" alt=\"logo\"></h1></a>
\t\t\t</div>
\t\t\t<div class=\"collapse navbar-collapse\">
\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">
\t\t\t\t\t<li class=\"scroll active\"><a href=\"#home\">Home</a></li>
\t\t\t\t\t<li class=\"scroll\"><a href=\"#about-us\">About Us</a></li>
\t\t\t\t\t<li class=\"scroll\"><a href=\"#innovation\">Innovation</a></li>
\t\t\t\t\t<li class=\"scroll\"><a href=\"#platform\">Platform</a></li>
\t\t\t\t\t<li class=\"scroll\"><a href=\"#partners\">Partners</a></li>
\t\t\t\t\t<li class=\"scroll\"><a href=\"#contact-us\">Contact</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "partials/navigation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 11,  19 => 1,);
    }
}
/* <div id="navigation">*/
/* 	<div class="navbar" role="banner">*/
/* 		<div class="container">*/
/* 			<div class="navbar-header">*/
/* 				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/* 					<span class="sr-only">Toggle navigation</span>*/
/* 					<span class="icon-bar"></span>*/
/* 					<span class="icon-bar"></span>*/
/* 					<span class="icon-bar"></span>*/
/* 				</button>*/
/* 				<a class="navbar-brand" href="{{ base_url }}"><h1><img class="img-responsive" src="{{ url('theme://img/logo_black_small.png') }}" alt="logo"></h1></a>*/
/* 			</div>*/
/* 			<div class="collapse navbar-collapse">*/
/* 				<ul class="nav navbar-nav navbar-right">*/
/* 					<li class="scroll active"><a href="#home">Home</a></li>*/
/* 					<li class="scroll"><a href="#about-us">About Us</a></li>*/
/* 					<li class="scroll"><a href="#innovation">Innovation</a></li>*/
/* 					<li class="scroll"><a href="#platform">Platform</a></li>*/
/* 					<li class="scroll"><a href="#partners">Partners</a></li>*/
/* 					<li class="scroll"><a href="#contact-us">Contact</a></li>*/
/* 				</ul>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* </div>*/
/* */
