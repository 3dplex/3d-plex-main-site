<?php

/* modular/contact.html.twig */
class __TwigTemplate_049e5ce4834a218abbe6d535c9ee9278ee2c6d94b4d367635736e9f183f94a5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"contact-us\" class=\"parallax-section\">
    <div class=\"parallax-content\">
        <div class=\"color-overlay\"></div>
        <div class=\"container\">
            <div class=\"row text-center section-title\">
                <div class=\"col-sm-8 col-sm-offset-2 wow pulse\">
                    <h2>";
        // line 7
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "title", array());
        echo "</h2>
                    <p>";
        // line 8
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "</p>
                </div>
            </div>
            <div class=\"col-sm-12\">
                <div class=\"contact-info wow fadeIn\">
                    <h3>";
        // line 13
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "infoheader", array());
        echo "</h3>
                    <ul>
                        <li><i class=\"fa fa-envelope\"></i><a href=\"mailto:";
        // line 15
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->safeEmailFilter("info@3dplex.io");
        echo "\" onclick=\"ga('send', 'pageview', '/info-email');\"> ";
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "email", array());
        echo "</a></li>
                    </ul>
                    <div class=\"row\">
                        <div id=\"subbed\" class=\"hide\">
                            <h4>";
        // line 19
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "thankyousign", array());
        echo "</h4>
                        </div>
                        <div id=\"sub\">
                            <h4>";
        // line 22
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "signmessage", array());
        echo "</h4>
                            <form id=\"contact-form\" name=\"contact-form\" role=\"form\" enctype=\"multipart/form-data\" autocomplete=\"off\">
                                <div class=\"row\">
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <input type=\"email\" name=\"email\" class=\"form-control\" required=\"required\" placeholder=\"Email\" required>
                                        </div>
                                    </div>
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <button type=\"submit\" class=\"btn btn-submit form-submit\">";
        // line 32
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "buttonmessage", array());
        echo "</button>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"mailchimp\">
                                    <input type=\"text\" name=\"";
        // line 37
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "mailchimpform", array());
        echo "\" tabindex=\"-1\" value=\"\">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class=\"social-icons\">
                        <h3>";
        // line 43
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "socialtitle", array());
        echo "</h3>
                        <ul>
                            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "socials", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["social"]) {
            // line 46
            echo "                            <li><a href=\"";
            echo $this->getAttribute($context["social"], "url", array());
            echo "\" title=\"";
            echo $this->getAttribute($context["social"], "url", array());
            echo "\" target=\"_blank\" onclick=\"";
            echo $this->getAttribute($context["social"], "action", array());
            echo "\"><i class=\"fa fa-";
            echo $this->getAttribute($context["social"], "icon", array());
            echo "\"></i></a></li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['social'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 48,  98 => 46,  94 => 45,  89 => 43,  80 => 37,  72 => 32,  59 => 22,  53 => 19,  44 => 15,  39 => 13,  31 => 8,  27 => 7,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"contact-us\" class=\"parallax-section\">
    <div class=\"parallax-content\">
        <div class=\"color-overlay\"></div>
        <div class=\"container\">
            <div class=\"row text-center section-title\">
                <div class=\"col-sm-8 col-sm-offset-2 wow pulse\">
                    <h2>{{ page.header.title }}</h2>
                    <p>{{ page.content }}</p>
                </div>
            </div>
            <div class=\"col-sm-12\">
                <div class=\"contact-info wow fadeIn\">
                    <h3>{{ page.header.infoheader }}</h3>
                    <ul>
                        <li><i class=\"fa fa-envelope\"></i><a href=\"mailto:{{ 'info@3dplex.io'|safe_email }}\" onclick=\"ga('send', 'pageview', '/info-email');\"> {{ page.header.email }}</a></li>
                    </ul>
                    <div class=\"row\">
                        <div id=\"subbed\" class=\"hide\">
                            <h4>{{ page.header.thankyousign }}</h4>
                        </div>
                        <div id=\"sub\">
                            <h4>{{ page.header.signmessage }}</h4>
                            <form id=\"contact-form\" name=\"contact-form\" role=\"form\" enctype=\"multipart/form-data\" autocomplete=\"off\">
                                <div class=\"row\">
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <input type=\"email\" name=\"email\" class=\"form-control\" required=\"required\" placeholder=\"Email\" required>
                                        </div>
                                    </div>
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <button type=\"submit\" class=\"btn btn-submit form-submit\">{{ page.header.buttonmessage }}</button>
                                        </div>
                                    </div>
                                </div>
                                <div class=\"mailchimp\">
                                    <input type=\"text\" name=\"{{ page.header.mailchimpform }}\" tabindex=\"-1\" value=\"\">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class=\"social-icons\">
                        <h3>{{ page.header.socialtitle }}</h3>
                        <ul>
                            {% for social in page.header.socials %}
                            <li><a href=\"{{ social.url }}\" title=\"{{ social.url }}\" target=\"_blank\" onclick=\"{{ social.action }}\"><i class=\"fa fa-{{ social.icon }}\"></i></a></li>
                            {% endfor %}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
", "modular/contact.html.twig", "/var/www/html/landing/user/themes/plex/templates/modular/contact.html.twig");
    }
}
