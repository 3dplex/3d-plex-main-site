<?php

/* partials/footer.html.twig */
class __TwigTemplate_40320b20bbc89fa0ab3e6f7f6ecb5708b3ddfd89e290bd4ce7d072b3dc58cf32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer id=\"footer\">
    <div class=\"container text-center\">
        <p>&copy; ";
        // line 3
        echo twig_date_format_filter($this->env, "now", "Y");
        echo " <a href=\"";
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "\">3dplex.io</a>. All rights reserved.</p>
    </div>
    <a data-scroll href=\"#home\" class=\"to-top\"><i class=\"fa fa-angle-up\"></i></a>
</footer>
";
    }

    public function getTemplateName()
    {
        return "partials/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }
}
/* <footer id="footer">*/
/*     <div class="container text-center">*/
/*         <p>&copy; {{ "now"|date('Y') }} <a href="{{ base_url }}">3dplex.io</a>. All rights reserved.</p>*/
/*     </div>*/
/*     <a data-scroll href="#home" class="to-top"><i class="fa fa-angle-up"></i></a>*/
/* </footer>*/
/* */
