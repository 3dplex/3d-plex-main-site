<?php

/* modular/partners.html.twig */
class __TwigTemplate_7d80d92b769fc8b4cfe6ebd9278811234e0d003198d425980adc560a7ad6c7b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"partners\" class=\"padding-top padding-bottom\">
    <div class=\"container\">
        <div class=\"row text-center section-title\">
            <div class=\"col-sm-8 col-sm-offset-2 wow pulse\">
                <h2>";
        // line 5
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "title", array());
        echo "</h2>
                ";
        // line 6
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "
            </div>
        </div>
        <div class=\"team-members text-center\">
            <div class=\"row\">
                <div class=\"col-sm-6 col-md-3 center-block middle wow fadeIn\">
                    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "partners", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["partner"]) {
            // line 13
            echo "                    <div class=\"member\">
                        <div class=\"member-image\">
                            <a href=\"";
            // line 15
            echo $this->getAttribute($context["partner"], "link", array());
            echo "\" target=\"_blank\" title=\"";
            echo $this->getAttribute($context["partner"], "text", array());
            echo "\"><img class=\"img-responsive\" src=\"";
            echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), $this->getAttribute($context["partner"], "img", array()), array(), "array"), "url", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["partner"], "text", array());
            echo "\"></a>
                        </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['partner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                </div>
            </div>
        </div>
    </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/partners.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 19,  46 => 15,  42 => 13,  38 => 12,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* <section id="partners" class="padding-top padding-bottom">*/
/*     <div class="container">*/
/*         <div class="row text-center section-title">*/
/*             <div class="col-sm-8 col-sm-offset-2 wow pulse">*/
/*                 <h2>{{ page.header.title }}</h2>*/
/*                 {{ page.content }}*/
/*             </div>*/
/*         </div>*/
/*         <div class="team-members text-center">*/
/*             <div class="row">*/
/*                 <div class="col-sm-6 col-md-3 center-block middle wow fadeIn">*/
/*                     {% for partner in page.header.partners %}*/
/*                     <div class="member">*/
/*                         <div class="member-image">*/
/*                             <a href="{{ partner.link }}" target="_blank" title="{{ partner.text }}"><img class="img-responsive" src="{{ page.media[partner.img].url }}" alt="{{ partner.text }}"></a>*/
/*                         </div>*/
/*                     </div>*/
/*                     {% endfor %}*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* */
