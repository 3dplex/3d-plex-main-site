<?php

/* modular/platform.html.twig */
class __TwigTemplate_a5043930559f132aaa4b19c275306256c490ff4277fd2253bbeca402b54870d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"platform\" class=\"padding-top\">
    <div class=\"container text-center\">
        <div class=\"row section-title\">
            <div class=\"col-sm-8 col-sm-offset-2 wow pulse\">
                <h2>";
        // line 5
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "title", array());
        echo "</h2>
            </div>
        </div>
        <div class=\"section-image wow fadeIn\">
            <img class=\"img-responsive\" src=\"";
        // line 9
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), "4elements.jpg", array(), "array"), "url", array());
        echo "\" alt=\"4 elements\" />
        </div>
        <div class=\"row wow fadeIn\">
            ";
        // line 12
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "
        </div>
        <div class=\"section-image wow fadeIn\">
            <img class=\"img-responsive\" src=\"";
        // line 15
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), "micro.jpg", array(), "array"), "url", array());
        echo "\" alt=\"micro-factory\" />
        </div>
    </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/platform.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 15,  38 => 12,  32 => 9,  25 => 5,  19 => 1,);
    }
}
/* <section id="platform" class="padding-top">*/
/*     <div class="container text-center">*/
/*         <div class="row section-title">*/
/*             <div class="col-sm-8 col-sm-offset-2 wow pulse">*/
/*                 <h2>{{ page.header.title }}</h2>*/
/*             </div>*/
/*         </div>*/
/*         <div class="section-image wow fadeIn">*/
/*             <img class="img-responsive" src="{{ page.media['4elements.jpg'].url }}" alt="4 elements" />*/
/*         </div>*/
/*         <div class="row wow fadeIn">*/
/*             {{ page.content }}*/
/*         </div>*/
/*         <div class="section-image wow fadeIn">*/
/*             <img class="img-responsive" src="{{ page.media['micro.jpg'].url }}" alt="micro-factory" />*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* */
