<?php

/* modular/about.html.twig */
class __TwigTemplate_f47b81630b645ceab1191d169018eb714b0a50672a7b9c3a1815c19aeef4c0c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"about-us\" class=\"padding-top off-white\">
\t<div class=\"container text-center\">
\t\t<div class=\"row section-title\">
\t\t\t<div class=\"col-sm-8 col-sm-offset-2 wow pulse\">
\t\t\t\t<h2>";
        // line 5
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "title", array());
        echo "</h2>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "sections", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
            // line 10
            echo "\t\t\t<div class=\"col-md-4 col-sm-4 about-content wow fadeIn\">
\t\t\t\t<div class=\"about-icon\">
\t\t\t\t\t<i class=\"fa fa-";
            // line 12
            echo $this->getAttribute($context["section"], "icon", array());
            echo "\"></i>
\t\t\t\t</div>
\t\t\t\t<div class=\"about-text\">
\t\t\t\t\t<h3>";
            // line 15
            echo $this->getAttribute($context["section"], "title", array());
            echo "</h3>
\t\t\t\t\t";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["section"], "paragraphs", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["text"]) {
                // line 17
                echo "\t\t\t\t\t<p>";
                echo $this->getAttribute($context["text"], "paragraph", array());
                echo "</p>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['text'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "\t\t</div>
\t</div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 22,  63 => 19,  54 => 17,  50 => 16,  46 => 15,  40 => 12,  36 => 10,  32 => 9,  25 => 5,  19 => 1,);
    }
}
/* <section id="about-us" class="padding-top off-white">*/
/* 	<div class="container text-center">*/
/* 		<div class="row section-title">*/
/* 			<div class="col-sm-8 col-sm-offset-2 wow pulse">*/
/* 				<h2>{{ page.header.title }}</h2>*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="row">*/
/* 			{% for section in page.header.sections %}*/
/* 			<div class="col-md-4 col-sm-4 about-content wow fadeIn">*/
/* 				<div class="about-icon">*/
/* 					<i class="fa fa-{{ section.icon }}"></i>*/
/* 				</div>*/
/* 				<div class="about-text">*/
/* 					<h3>{{ section.title }}</h3>*/
/* 					{% for text in section.paragraphs %}*/
/* 					<p>{{ text.paragraph }}</p>*/
/* 					{% endfor %}*/
/* 				</div>*/
/* 			</div>*/
/* 			{% endfor %}*/
/* 		</div>*/
/* 	</div>*/
/* </section>*/
/* */
