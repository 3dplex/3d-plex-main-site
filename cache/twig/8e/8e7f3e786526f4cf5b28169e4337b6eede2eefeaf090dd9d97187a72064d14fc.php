<?php

/* partials/navigation.html.twig */
class __TwigTemplate_08482a0a6df32bc2e79e96c1daeca6d2ad638590a0f715ee35b3096d4f7d75b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"navigation\">
\t<div class=\"navbar\" role=\"banner\">
\t\t<div class=\"container\">
\t\t\t<div class=\"navbar-header\">
\t\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t</button>
\t\t\t\t<a class=\"navbar-brand\" href=\"";
        // line 11
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "\"><h1><img class=\"img-responsive\" src=\"";
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->urlFunc("theme://img/logo_black_small.png");
        echo "\" alt=\"logo\"></h1></a>
\t\t\t</div>
\t\t\t<div class=\"collapse navbar-collapse\">
\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">
<!--\t\t\t\t\t<li class=\"scroll active\"><a href=\"#home\">Home</a></li>-->
<!--\t\t\t\t\t<li class=\"scroll\"><a href=\"#about-us\">About Us</a></li>-->
<!--\t\t\t\t\t<li class=\"scroll\"><a href=\"#innovation\">Innovation</a></li>-->
<!--\t\t\t\t\t<li class=\"scroll\"><a href=\"#platform\">Platform</a></li>-->
<!--\t\t\t\t\t<li class=\"scroll\"><a href=\"#partners\">Partners</a></li>-->
\t\t\t\t\t<li class=\"scroll\"><a href=\"#contact-us\">Contact</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "partials/navigation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 11,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"navigation\">
\t<div class=\"navbar\" role=\"banner\">
\t\t<div class=\"container\">
\t\t\t<div class=\"navbar-header\">
\t\t\t\t<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
\t\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t</button>
\t\t\t\t<a class=\"navbar-brand\" href=\"{{ base_url }}\"><h1><img class=\"img-responsive\" src=\"{{ url('theme://img/logo_black_small.png') }}\" alt=\"logo\"></h1></a>
\t\t\t</div>
\t\t\t<div class=\"collapse navbar-collapse\">
\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">
<!--\t\t\t\t\t<li class=\"scroll active\"><a href=\"#home\">Home</a></li>-->
<!--\t\t\t\t\t<li class=\"scroll\"><a href=\"#about-us\">About Us</a></li>-->
<!--\t\t\t\t\t<li class=\"scroll\"><a href=\"#innovation\">Innovation</a></li>-->
<!--\t\t\t\t\t<li class=\"scroll\"><a href=\"#platform\">Platform</a></li>-->
<!--\t\t\t\t\t<li class=\"scroll\"><a href=\"#partners\">Partners</a></li>-->
\t\t\t\t\t<li class=\"scroll\"><a href=\"#contact-us\">Contact</a></li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
", "partials/navigation.html.twig", "/var/www/html/landing/user/themes/plex/templates/partials/navigation.html.twig");
    }
}
