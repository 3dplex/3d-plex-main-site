<?php

/* modular/intro.html.twig */
class __TwigTemplate_9260f5e4587742649ef349c10b782453a11417c7059f2fc80b3b6652daa55381 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navigation' => array($this, 'block_navigation'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"home\">
\t<div id=\"home-intro\">
\t\t<img id=\"logo\" class=\"img-responsive\" src=\"";
        // line 3
        echo $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), "images", array()), "logo_small.png", array(), "array"), "url", array());
        echo "\" alt=\"logo\">
\t\t<div class=\"carousel-inner\">
\t\t\t<div class=\"item active\" data-vide-bg=\"mp4: ";
        // line 5
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), "3dplexintro2.mp4", array(), "array"), "url", array());
        echo "\" poster=\"";
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), "3dplexintro2.png", array(), "array"), "url", array());
        echo "\" data-vide-options=\"posterType: png\">
\t\t\t\t<div class=\"carousel-caption\">
\t\t\t\t\t<h2>";
        // line 7
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "title", array());
        echo "</h2>
\t\t\t\t\t<div id=\"js-rotating\">
\t\t\t\t\t\t";
        // line 9
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<a data-scroll class=\"smooth-scroll\" href=\"#navigation\">
\t\t<div class=\"mouse-icon\">
\t\t\t<div class=\"wheel\"></div>
\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t</div>
\t</a>
</div>

";
        // line 23
        $this->displayBlock('navigation', $context, $blocks);
    }

    public function block_navigation($context, array $blocks = array())
    {
        // line 24
        echo "\t";
        $this->loadTemplate("partials/navigation.html.twig", "modular/intro.html.twig", 24)->display($context);
    }

    public function getTemplateName()
    {
        return "modular/intro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 24,  58 => 23,  41 => 9,  36 => 7,  29 => 5,  24 => 3,  20 => 1,);
    }
}
/* <div id="home">*/
/* 	<div id="home-intro">*/
/* 		<img id="logo" class="img-responsive" src="{{ page.media.images['logo_small.png'].url }}" alt="logo">*/
/* 		<div class="carousel-inner">*/
/* 			<div class="item active" data-vide-bg="mp4: {{ page.media['3dplexintro2.mp4'].url }}" poster="{{ page.media['3dplexintro2.png'].url }}" data-vide-options="posterType: png">*/
/* 				<div class="carousel-caption">*/
/* 					<h2>{{ page.header.title }}</h2>*/
/* 					<div id="js-rotating">*/
/* 						{{ page.content }}*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	<a data-scroll class="smooth-scroll" href="#navigation">*/
/* 		<div class="mouse-icon">*/
/* 			<div class="wheel"></div>*/
/* 			<i class="fa fa-angle-down"></i>*/
/* 		</div>*/
/* 	</a>*/
/* </div>*/
/* */
/* {% block navigation %}*/
/* 	{% include 'partials/navigation.html.twig' %}*/
/* {% endblock %}*/
/* */
