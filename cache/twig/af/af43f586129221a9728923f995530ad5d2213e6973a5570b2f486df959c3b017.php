<?php

/* partials/metadata.html.twig */
class __TwigTemplate_8e58605431748caa7df76e34c52b98a97f0529de28def6c2a65699e9662580df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "metadata", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["meta"]) {
            // line 2
            echo "<meta name=\"";
            echo $this->getAttribute($context["meta"], "name", array());
            echo "\" content=\"";
            echo $this->getAttribute($context["meta"], "content", array());
            echo "\">
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['meta'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "partials/metadata.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 2,  19 => 1,);
    }
}
/* {% for meta in page.metadata %}*/
/* <meta name="{{ meta.name }}" content="{{ meta.content }}">*/
/* {% endfor %}*/
/* */
