<?php

/* modular/promo-two.html.twig */
class __TwigTemplate_287a5fcf35b89fbf217e5df05480282360cda5a5180eeec5f98c8e96923f69b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"promo-two\" class=\"parallax-section\">
    <div class=\"parallax-content\">
        <div class=\"container text-center\">
            <div class=\"row\">
                <div class=\"col-sm-12\">
                    <h3>We are a team of engineers</h3>
                    ";
        // line 7
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "
                    <a href=\"https://twitter.com/intent/tweet?screen_name=3DPlexLabs\" class=\"btn btn-primary\" data-related=\"3DPlexLabs\" onclick=\"ga('send', 'pageview', '/tweet-us');\">Tweet Us</a>
                    <script>
                        ! function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0],
                                p = /^http:/.test(d.location) ? 'http' : 'https';
                            if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = p + '://platform.twitter.com/widgets.js';
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }(document, 'script', 'twitter-wjs');
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "modular/promo-two.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 7,  19 => 1,);
    }
}
/* <div id="promo-two" class="parallax-section">*/
/*     <div class="parallax-content">*/
/*         <div class="container text-center">*/
/*             <div class="row">*/
/*                 <div class="col-sm-12">*/
/*                     <h3>We are a team of engineers</h3>*/
/*                     {{ page.content }}*/
/*                     <a href="https://twitter.com/intent/tweet?screen_name=3DPlexLabs" class="btn btn-primary" data-related="3DPlexLabs" onclick="ga('send', 'pageview', '/tweet-us');">Tweet Us</a>*/
/*                     <script>*/
/*                         ! function (d, s, id) {*/
/*                             var js, fjs = d.getElementsByTagName(s)[0],*/
/*                                 p = /^http:/.test(d.location) ? 'http' : 'https';*/
/*                             if (!d.getElementById(id)) {*/
/*                                 js = d.createElement(s);*/
/*                                 js.id = id;*/
/*                                 js.src = p + '://platform.twitter.com/widgets.js';*/
/*                                 fjs.parentNode.insertBefore(js, fjs);*/
/*                             }*/
/*                         }(document, 'script', 'twitter-wjs');*/
/*                     </script>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
