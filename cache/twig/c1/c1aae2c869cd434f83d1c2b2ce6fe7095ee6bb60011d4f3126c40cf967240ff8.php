<?php

/* modular/innovation.html.twig */
class __TwigTemplate_d343427d616cbb9807f2578d0b715f7f9d4cbe1bb8a2a22a0b59f54b31030c5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"innovation\" class=\"padding-top\">
    <div class=\"container text-center\">
        <div class=\"row section-title\">
            <div class=\"col-sm-8 col-sm-offset-2 wow pulse\">
                <h2>";
        // line 5
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "title", array());
        echo "</h2>
                ";
        // line 6
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "
            </div>
        </div>
        <div class=\"bout-us-image wow fadeIn\">
            <img class=\"img-responsive\" src=\"";
        // line 10
        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "media", array()), "360-innovation.png", array(), "array"), "url", array());
        echo "\" alt=\"360-innovation\" />
        </div>
    </div>
</section>
";
    }

    public function getTemplateName()
    {
        return "modular/innovation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 10,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* <section id="innovation" class="padding-top">*/
/*     <div class="container text-center">*/
/*         <div class="row section-title">*/
/*             <div class="col-sm-8 col-sm-offset-2 wow pulse">*/
/*                 <h2>{{ page.header.title }}</h2>*/
/*                 {{ page.content }}*/
/*             </div>*/
/*         </div>*/
/*         <div class="bout-us-image wow fadeIn">*/
/*             <img class="img-responsive" src="{{ page.media['360-innovation.png'].url }}" alt="360-innovation" />*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* */
