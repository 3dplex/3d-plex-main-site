<?php

/* modular/promo-one.html.twig */
class __TwigTemplate_52a6ef62a6253d5f2a892e8159f025d3c456a84564b91e33fef9be1ae242e532 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"promo-one\" class=\"parallax-section\">
    <div class=\"parallax-content\">
        <div class=\"color-overlay\"></div>
        <div class=\"container text-center\">
            <h2>";
        // line 5
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "title", array());
        echo "</h2>
            ";
        // line 6
        echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "modular/promo-one.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* <div id="promo-one" class="parallax-section">*/
/*     <div class="parallax-content">*/
/*         <div class="color-overlay"></div>*/
/*         <div class="container text-center">*/
/*             <h2>{{ page.header.title }}</h2>*/
/*             {{ page.content }}*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
