<?php

/* modular/testimonial.html.twig */
class __TwigTemplate_da431c311c34ee5f30f10ccad0696d863f5e04f5c15207d174bfe209337d77e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"testimonial\" class=\"parallax-section\">
    <div class=\"parallax-content\">
        <a class=\"testimony-left\" href=\"#testimonial-carousel\" data-slide=\"prev\"><i class=\"fa fa-angle-left\"></i></a>
        <a class=\"testimony-right\" href=\"#testimonial-carousel\" data-slide=\"next\"><i class=\"fa fa-angle-right\"></i></a>
        <div class=\"color-overlay\"></div>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-8 col-sm-offset-2\">
                    <div id=\"testimonial-carousel\" class=\"carousel slide\" data-ride=\"carousel\">
                        <div class=\"carousel-inner text-center\">
                            ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "sections", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
            // line 12
            echo "                            <div class=\"item ";
            if ($this->getAttribute($context["section"], "active", array())) {
                echo " active ";
            }
            echo "\">
                                <h3>";
            // line 13
            echo $this->getAttribute($context["section"], "title", array());
            echo "</h3>
                                ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["section"], "paragraphs", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["text"]) {
                // line 15
                echo "                                <p>";
                echo $this->getAttribute($context["text"], "paragraph", array());
                echo "</p>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['text'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "                            </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "modular/testimonial.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 19,  59 => 17,  50 => 15,  46 => 14,  42 => 13,  35 => 12,  31 => 11,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"testimonial\" class=\"parallax-section\">
    <div class=\"parallax-content\">
        <a class=\"testimony-left\" href=\"#testimonial-carousel\" data-slide=\"prev\"><i class=\"fa fa-angle-left\"></i></a>
        <a class=\"testimony-right\" href=\"#testimonial-carousel\" data-slide=\"next\"><i class=\"fa fa-angle-right\"></i></a>
        <div class=\"color-overlay\"></div>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-8 col-sm-offset-2\">
                    <div id=\"testimonial-carousel\" class=\"carousel slide\" data-ride=\"carousel\">
                        <div class=\"carousel-inner text-center\">
                            {% for section in page.header.sections %}
                            <div class=\"item {% if section.active %} active {% endif %}\">
                                <h3>{{ section.title }}</h3>
                                {% for text in section.paragraphs %}
                                <p>{{ text.paragraph }}</p>
                                {% endfor %}
                            </div>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
", "modular/testimonial.html.twig", "/var/www/html/landing/user/themes/plex/templates/modular/testimonial.html.twig");
    }
}
