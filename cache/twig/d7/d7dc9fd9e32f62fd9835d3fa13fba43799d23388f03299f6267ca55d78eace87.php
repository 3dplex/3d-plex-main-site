<?php

/* partials/preloader.html.twig */
class __TwigTemplate_e992ca1d56f506d14ceeee7db643c8f619e47229a377680a87cb1de86cc3c71d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"preloader\">
    <div id=\"loaderImage\"></div>
</div>
";
    }

    public function getTemplateName()
    {
        return "partials/preloader.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"preloader\">
    <div id=\"loaderImage\"></div>
</div>
", "partials/preloader.html.twig", "/var/www/html/landing/user/themes/plex/templates/partials/preloader.html.twig");
    }
}
