<?php
<<<<<<< HEAD
=======
/**
 * @package    Grav.Common.Config
 *
 * @copyright  Copyright (C) 2014 - 2016 RocketTheme, LLC. All rights reserved.
 * @license    MIT License; see LICENSE file for details.
 */

>>>>>>> update grav cms
namespace Grav\Common\Config;

use Grav\Common\Data\Data;

<<<<<<< HEAD
/**
 * The Languages class contains configuration rules.
 *
 * @author RocketTheme
 * @license MIT
 */
=======
>>>>>>> update grav cms
class Languages extends Data
{
    public function checksum($checksum = null)
    {
        if ($checksum !== null) {
            $this->checksum = $checksum;
        }

        return $this->checksum;
    }

    public function modified($modified = null)
    {
        if ($modified !== null) {
            $this->modified = $modified;
        }

        return $this->modified;
    }

    public function reformat()
    {
        if (isset($this->items['plugins'])) {
            $this->items = array_merge_recursive($this->items, $this->items['plugins']);
            unset($this->items['plugins']);
        }
    }

    public function mergeRecursive(array $data)
    {
        $this->items = array_merge_recursive($this->items, $data);
    }
}
