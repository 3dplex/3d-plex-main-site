<?php
<<<<<<< HEAD
namespace Grav\Common\GPM;

use Grav\Common\GravTrait;
use Grav\Common\Iterator;

abstract class AbstractCollection extends Iterator {

    use GravTrait;

=======
/**
 * @package    Grav.Common.GPM
 *
 * @copyright  Copyright (C) 2014 - 2016 RocketTheme, LLC. All rights reserved.
 * @license    MIT License; see LICENSE file for details.
 */

namespace Grav\Common\GPM;

use Grav\Common\Iterator;

abstract class AbstractCollection extends Iterator
{
>>>>>>> update grav cms
    public function toJson()
    {
        $items = [];

        foreach ($this->items as $name => $package) {
            $items[$name] = $package->toArray();
        }

        return json_encode($items);
    }

    public function toArray()
    {
        $items = [];

        foreach ($this->items as $name => $package) {
            $items[$name] = $package->toArray();
        }

        return $items;
    }
}
