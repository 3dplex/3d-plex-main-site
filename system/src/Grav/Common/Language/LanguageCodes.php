<?php
<<<<<<< HEAD
namespace Grav\Common\Language;

/**
 * Language and translation functionality for Grav
 */
class LanguageCodes
{
    protected static $codes = [
        "ab" => [
            "name"       => "Abkhaz",
            "nativeName" => "аҧсуа"
        ],
        "aa" => [
            "name"       => "Afar",
            "nativeName" => "Afaraf"
        ],
        "af" => [
            "name"       => "Afrikaans",
            "nativeName" => "Afrikaans"
        ],
        "ak" => [
            "name"       => "Akan",
            "nativeName" => "Akan"
        ],
        "sq" => [
            "name"       => "Albanian",
            "nativeName" => "Shqip"
        ],
        "am" => [
            "name"       => "Amharic",
            "nativeName" => "አማርኛ"
        ],
        "ar" => [
            "name"       => "Arabic",
            "nativeName" => "العربية"
        ],
        "an" => [
            "name"       => "Aragonese",
            "nativeName" => "Aragonés"
        ],
        "hy" => [
            "name"       => "Armenian",
            "nativeName" => "Հայերեն"
        ],
        "as" => [
            "name"       => "Assamese",
            "nativeName" => "অসমীয়া"
        ],
        "av" => [
            "name"       => "Avaric",
            "nativeName" => "авар мацӀ"
        ],
        "ae" => [
            "name"       => "Avestan",
            "nativeName" => "avesta"
        ],
        "ay" => [
            "name"       => "Aymara",
            "nativeName" => "aymar aru"
        ],
        "az" => [
            "name"       => "Azerbaijani",
            "nativeName" => "azərbaycan dili"
        ],
        "bm" => [
            "name"       => "Bambara",
            "nativeName" => "bamanankan"
        ],
        "ba" => [
            "name"       => "Bashkir",
            "nativeName" => "башҡорт теле"
        ],
        "eu" => [
            "name"       => "Basque",
            "nativeName" => "euskara"
        ],
        "be" => [
            "name"       => "Belarusian",
            "nativeName" => "Беларуская"
        ],
        "bn" => [
            "name"       => "Bengali",
            "nativeName" => "বাংলা"
        ],
        "bh" => [
            "name"       => "Bihari",
            "nativeName" => "भोजपुरी"
        ],
        "bi" => [
            "name"       => "Bislama",
            "nativeName" => "Bislama"
        ],
        "bs" => [
            "name"       => "Bosnian",
            "nativeName" => "bosanski jezik"
        ],
        "br" => [
            "name"       => "Breton",
            "nativeName" => "brezhoneg"
        ],
        "bg" => [
            "name"       => "Bulgarian",
            "nativeName" => "български език"
        ],
        "my" => [
            "name"       => "Burmese",
            "nativeName" => "ဗမာစာ"
        ],
        "ca" => [
            "name"       => "Catalan",
            "nativeName" => "Català"
        ],
        "ch" => [
            "name"       => "Chamorro",
            "nativeName" => "Chamoru"
        ],
        "ce" => [
            "name"       => "Chechen",
            "nativeName" => "нохчийн мотт"
        ],
        "ny" => [
            "name"       => "Chichewa",
            "nativeName" => "chiCheŵa"
        ],
        "zh" => [
            "name"       => "Chinese",
            "nativeName" => "中文"
        ],
        "cv" => [
            "name"       => "Chuvash",
            "nativeName" => "чӑваш чӗлхи"
        ],
        "kw" => [
            "name"       => "Cornish",
            "nativeName" => "Kernewek"
        ],
        "co" => [
            "name"       => "Corsican",
            "nativeName" => "corsu"
        ],
        "cr" => [
            "name"       => "Cree",
            "nativeName" => "ᓀᐦᐃᔭᐍᐏᐣ"
        ],
        "hr" => [
            "name"       => "Croatian",
            "nativeName" => "hrvatski"
        ],
        "cs" => [
            "name"       => "Czech",
            "nativeName" => "česky"
        ],
        "da" => [
            "name"       => "Danish",
            "nativeName" => "dansk"
        ],
        "dv" => [
            "name"       => "Divehi",
            "nativeName" => "ދިވެހި"
        ],
        "nl" => [
            "name"       => "Dutch",
            "nativeName" => "Nederlands"
        ],
        "en" => [
            "name"       => "English",
            "nativeName" => "English"
        ],
        "eo" => [
            "name"       => "Esperanto",
            "nativeName" => "Esperanto"
        ],
        "et" => [
            "name"       => "Estonian",
            "nativeName" => "eesti"
        ],
        "ee" => [
            "name"       => "Ewe",
            "nativeName" => "Eʋegbe"
        ],
        "fo" => [
            "name"       => "Faroese",
            "nativeName" => "føroyskt"
        ],
        "fj" => [
            "name"       => "Fijian",
            "nativeName" => "vosa Vakaviti"
        ],
        "fi" => [
            "name"       => "Finnish",
            "nativeName" => "suomi"
        ],
        "fr" => [
            "name"       => "French",
            "nativeName" => "Français"
        ],
        "ff" => [
            "name"       => "Fula",
            "nativeName" => "Fulfulde"
        ],
        "gl" => [
            "name"       => "Galician",
            "nativeName" => "Galego"
        ],
        "ka" => [
            "name"       => "Georgian",
            "nativeName" => "ქართული"
        ],
        "de" => [
            "name"       => "German",
            "nativeName" => "Deutsch"
        ],
        "el" => [
            "name"       => "Greek",
            "nativeName" => "Ελληνικά"
        ],
        "gn" => [
            "name"       => "Guaraní",
            "nativeName" => "Avañeẽ"
        ],
        "gu" => [
            "name"       => "Gujarati",
            "nativeName" => "ગુજરાતી"
        ],
        "ht" => [
            "name"       => "Haitian",
            "nativeName" => "Kreyòl ayisyen"
        ],
        "ha" => [
            "name"       => "Hausa",
            "nativeName" => "هَوُسَ"
        ],
        "he" => [
            "name"       => "Hebrew",
            "nativeName" => "עברית"
        ],
        "hz" => [
            "name"       => "Herero",
            "nativeName" => "Otjiherero"
        ],
        "hi" => [
            "name"       => "Hindi",
            "nativeName" => "हिन्दी"
        ],
        "ho" => [
            "name"       => "Hiri Motu",
            "nativeName" => "Hiri Motu"
        ],
        "hu" => [
            "name"       => "Hungarian",
            "nativeName" => "Magyar"
        ],
        "ia" => [
            "name"       => "Interlingua",
            "nativeName" => "Interlingua"
        ],
        "id" => [
            "name"       => "Indonesian",
            "nativeName" => "Bahasa Indonesia"
        ],
        "ie" => [
            "name"       => "Interlingue",
            "nativeName" => "Interlingue"
        ],
        "ga" => [
            "name"       => "Irish",
            "nativeName" => "Gaeilge"
        ],
        "ig" => [
            "name"       => "Igbo",
            "nativeName" => "Asụsụ Igbo"
        ],
        "ik" => [
            "name"       => "Inupiaq",
            "nativeName" => "Iñupiaq"
        ],
        "io" => [
            "name"       => "Ido",
            "nativeName" => "Ido"
        ],
        "is" => [
            "name"       => "Icelandic",
            "nativeName" => "Íslenska"
        ],
        "it" => [
            "name"       => "Italian",
            "nativeName" => "Italiano"
        ],
        "iu" => [
            "name"       => "Inuktitut",
            "nativeName" => "ᐃᓄᒃᑎᑐᑦ"
        ],
        "ja" => [
            "name"       => "Japanese",
            "nativeName" => "日本語"
        ],
        "jv" => [
            "name"       => "Javanese",
            "nativeName" => "basa Jawa"
        ],
        "kl" => [
            "name"       => "Kalaallisut",
            "nativeName" => "kalaallisut"
        ],
        "kn" => [
            "name"       => "Kannada",
            "nativeName" => "ಕನ್ನಡ"
        ],
        "kr" => [
            "name"       => "Kanuri",
            "nativeName" => "Kanuri"
        ],
        "ks" => [
            "name"       => "Kashmiri",
            "nativeName" => "कश्मीरी"
        ],
        "kk" => [
            "name"       => "Kazakh",
            "nativeName" => "Қазақ тілі"
        ],
        "km" => [
            "name"       => "Khmer",
            "nativeName" => "ភាសាខ្មែរ"
        ],
        "ki" => [
            "name"       => "Kikuyu",
            "nativeName" => "Gĩkũyũ"
        ],
        "rw" => [
            "name"       => "Kinyarwanda",
            "nativeName" => "Ikinyarwanda"
        ],
        "ky" => [
            "name"       => "Kirghiz",
            "nativeName" => "кыргыз тили"
        ],
        "kv" => [
            "name"       => "Komi",
            "nativeName" => "коми кыв"
        ],
        "kg" => [
            "name"       => "Kongo",
            "nativeName" => "KiKongo"
        ],
        "ko" => [
            "name"       => "Korean",
            "nativeName" => "한국어"
        ],
        "ku" => [
            "name"       => "Kurdish",
            "nativeName" => "كوردی‎"
        ],
        "kj" => [
            "name"       => "Kwanyama",
            "nativeName" => "Kuanyama"
        ],
        "la" => [
            "name"       => "Latin",
            "nativeName" => "latine"
        ],
        "lb" => [
            "name"       => "Luxembourgish",
            "nativeName" => "Lëtzebuergesch"
        ],
        "lg" => [
            "name"       => "Luganda",
            "nativeName" => "Luganda"
        ],
        "li" => [
            "name"       => "Limburgish",
            "nativeName" => "Limburgs"
        ],
        "ln" => [
            "name"       => "Lingala",
            "nativeName" => "Lingála"
        ],
        "lo" => [
            "name"       => "Lao",
            "nativeName" => "ພາສາລາວ"
        ],
        "lt" => [
            "name"       => "Lithuanian",
            "nativeName" => "lietuvių kalba"
        ],
        "lu" => [
            "name"       => "Luba-Katanga",
            "nativeName" => "Luba-Katanga"
        ],
        "lv" => [
            "name"       => "Latvian",
            "nativeName" => "latviešu valoda"
        ],
        "gv" => [
            "name"       => "Manx",
            "nativeName" => "Gaelg"
        ],
        "mk" => [
            "name"       => "Macedonian",
            "nativeName" => "македонски јазик"
        ],
        "mg" => [
            "name"       => "Malagasy",
            "nativeName" => "Malagasy fiteny"
        ],
        "ms" => [
            "name"       => "Malay",
            "nativeName" => "بهاس ملايو‎"
        ],
        "ml" => [
            "name"       => "Malayalam",
            "nativeName" => "മലയാളം"
        ],
        "mt" => [
            "name"       => "Maltese",
            "nativeName" => "Malti"
        ],
        "mi" => [
            "name"       => "Māori",
            "nativeName" => "te reo Māori"
        ],
        "mr" => [
            "name"       => "Marathi",
            "nativeName" => "मराठी"
        ],
        "mh" => [
            "name"       => "Marshallese",
            "nativeName" => "Kajin M̧ajeļ"
        ],
        "mn" => [
            "name"       => "Mongolian",
            "nativeName" => "монгол"
        ],
        "na" => [
            "name"       => "Nauru",
            "nativeName" => "Ekakairũ Naoero"
        ],
        "nv" => [
            "name"       => "Navajo",
            "nativeName" => "Diné bizaad"
        ],
        "nb" => [
            "name"       => "Norwegian Bokmål",
            "nativeName" => "Norsk bokmål"
        ],
        "nd" => [
            "name"       => "North Ndebele",
            "nativeName" => "isiNdebele"
        ],
        "ne" => [
            "name"       => "Nepali",
            "nativeName" => "नेपाली"
        ],
        "ng" => [
            "name"       => "Ndonga",
            "nativeName" => "Owambo"
        ],
        "nn" => [
            "name"       => "Norwegian Nynorsk",
            "nativeName" => "Norsk nynorsk"
        ],
        "no" => [
            "name"       => "Norwegian",
            "nativeName" => "Norsk"
        ],
        "ii" => [
            "name"       => "Nuosu",
            "nativeName" => "ꆈꌠ꒿ Nuosuhxop"
        ],
        "nr" => [
            "name"       => "South Ndebele",
            "nativeName" => "isiNdebele"
        ],
        "oc" => [
            "name"       => "Occitan",
            "nativeName" => "Occitan"
        ],
        "oj" => [
            "name"       => "Ojibwe, Ojibwa",
            "nativeName" => "ᐊᓂᔑᓈᐯᒧᐎᓐ"
        ],
        "cu" => [
            "name"       => "Church Slavic",
            "nativeName" => "ѩзыкъ словѣньскъ"
        ],
        "om" => [
            "name"       => "Oromo",
            "nativeName" => "Afaan Oromoo"
        ],
        "or" => [
            "name"       => "Oriya",
            "nativeName" => "ଓଡ଼ିଆ"
        ],
        "os" => [
            "name"       => "Ossetian",
            "nativeName" => "ирон æвзаг"
        ],
        "pa" => [
            "name"       => "Panjabi",
            "nativeName" => "ਪੰਜਾਬੀ"
        ],
        "pi" => [
            "name"       => "Pāli",
            "nativeName" => "पाऴि"
        ],
        "fa" => [
            "name"       => "Persian",
            "nativeName" => "فارسی"
        ],
        "pl" => [
            "name"       => "Polish",
            "nativeName" => "polski"
        ],
        "ps" => [
            "name"       => "Pashto",
            "nativeName" => "پښتو"
        ],
        "pt" => [
            "name"       => "Portuguese",
            "nativeName" => "Português"
        ],
        "qu" => [
            "name"       => "Quechua",
            "nativeName" => "Runa Simi"
        ],
        "rm" => [
            "name"       => "Romansh",
            "nativeName" => "rumantsch grischun"
        ],
        "rn" => [
            "name"       => "Kirundi",
            "nativeName" => "kiRundi"
        ],
        "ro" => [
            "name"       => "Romanian",
            "nativeName" => "română"
        ],
        "ru" => [
            "name"       => "Russian",
            "nativeName" => "Русский"
        ],
        "sa" => [
            "name"       => "Sanskrit",
            "nativeName" => "संस्कृतम्"
        ],
        "sc" => [
            "name"       => "Sardinian",
            "nativeName" => "sardu"
        ],
        "sd" => [
            "name"       => "Sindhi",
            "nativeName" => "सिन्धी"
        ],
        "se" => [
            "name"       => "Northern Sami",
            "nativeName" => "Davvisámegiella"
        ],
        "sm" => [
            "name"       => "Samoan",
            "nativeName" => "gagana faa Samoa"
        ],
        "sg" => [
            "name"       => "Sango",
            "nativeName" => "yângâ tî sängö"
        ],
        "sr" => [
            "name"       => "Serbian",
            "nativeName" => "српски језик"
        ],
        "gd" => [
            "name"       => "Scottish Gaelic",
            "nativeName" => "Gàidhlig"
        ],
        "sn" => [
            "name"       => "Shona",
            "nativeName" => "chiShona"
        ],
        "si" => [
            "name"       => "Sinhala",
            "nativeName" => "සිංහල"
        ],
        "sk" => [
            "name"       => "Slovak",
            "nativeName" => "slovenčina"
        ],
        "sl" => [
            "name"       => "Slovene",
            "nativeName" => "slovenščina"
        ],
        "so" => [
            "name"       => "Somali",
            "nativeName" => "Soomaaliga"
        ],
        "st" => [
            "name"       => "Southern Sotho",
            "nativeName" => "Sesotho"
        ],
        "es" => [
            "name"       => "Spanish",
            "nativeName" => "Español"
        ],
        "su" => [
            "name"       => "Sundanese",
            "nativeName" => "Basa Sunda"
        ],
        "sw" => [
            "name"       => "Swahili",
            "nativeName" => "Kiswahili"
        ],
        "ss" => [
            "name"       => "Swati",
            "nativeName" => "SiSwati"
        ],
        "sv" => [
            "name"       => "Swedish",
            "nativeName" => "svenska"
        ],
        "ta" => [
            "name"       => "Tamil",
            "nativeName" => "தமிழ்"
        ],
        "te" => [
            "name"       => "Telugu",
            "nativeName" => "తెలుగు"
        ],
        "tg" => [
            "name"       => "Tajik",
            "nativeName" => "тоҷикӣ"
        ],
        "th" => [
            "name"       => "Thai",
            "nativeName" => "ไทย"
        ],
        "ti" => [
            "name"       => "Tigrinya",
            "nativeName" => "ትግርኛ"
        ],
        "bo" => [
            "name"       => "Tibetan",
            "nativeName" => "བོད་ཡིག"
        ],
        "tk" => [
            "name"       => "Turkmen",
            "nativeName" => "Türkmen"
        ],
        "tl" => [
            "name"       => "Tagalog",
            "nativeName" => "Wikang Tagalog"
        ],
        "tn" => [
            "name"       => "Tswana",
            "nativeName" => "Setswana"
        ],
        "to" => [
            "name"       => "Tonga",
            "nativeName" => "faka Tonga"
        ],
        "tr" => [
            "name"       => "Turkish",
            "nativeName" => "Türkçe"
        ],
        "ts" => [
            "name"       => "Tsonga",
            "nativeName" => "Xitsonga"
        ],
        "tt" => [
            "name"       => "Tatar",
            "nativeName" => "татарча"
        ],
        "tw" => [
            "name"       => "Twi",
            "nativeName" => "Twi"
        ],
        "ty" => [
            "name"       => "Tahitian",
            "nativeName" => "Reo Tahiti"
        ],
        "ug" => [
            "name"       => "Uighur",
            "nativeName" => "Uyƣurqə"
        ],
        "uk" => [
            "name"       => "Ukrainian",
            "nativeName" => "українська"
        ],
        "ur" => [
            "name"       => "Urdu",
            "nativeName" => "اردو"
        ],
        "uz" => [
            "name"       => "Uzbek",
            "nativeName" => "zbek"
        ],
        "ve" => [
            "name"       => "Venda",
            "nativeName" => "Tshivenḓa"
        ],
        "vi" => [
            "name"       => "Vietnamese",
            "nativeName" => "Tiếng Việt"
        ],
        "vo" => [
            "name"       => "Volapük",
            "nativeName" => "Volapük"
        ],
        "wa" => [
            "name"       => "Walloon",
            "nativeName" => "Walon"
        ],
        "cy" => [
            "name"       => "Welsh",
            "nativeName" => "Cymraeg"
        ],
        "wo" => [
            "name"       => "Wolof",
            "nativeName" => "Wollof"
        ],
        "fy" => [
            "name"       => "Western Frisian",
            "nativeName" => "Frysk"
        ],
        "xh" => [
            "name"       => "Xhosa",
            "nativeName" => "isiXhosa"
        ],
        "yi" => [
            "name"       => "Yiddish",
            "nativeName" => "ייִדיש"
        ],
        "yo" => [
            "name"       => "Yoruba",
            "nativeName" => "Yorùbá"
        ],
        "za" => [
            "name"       => "Zhuang",
            "nativeName" => "Saɯ cueŋƅ"
        ]
=======
/**
 * @package    Grav.Common.Language
 *
 * @copyright  Copyright (C) 2014 - 2016 RocketTheme, LLC. All rights reserved.
 * @license    MIT License; see LICENSE file for details.
 */

namespace Grav\Common\Language;

class LanguageCodes
{
    protected static $codes = [
        'af'         => [ 'name' => 'Afrikaans',                 'nativeName' => 'Afrikaans' ],
        'ak'         => [ 'name' => 'Akan',                      'nativeName' => 'Akan' ], // unverified native name
        'ast'        => [ 'name' => 'Asturian',                  'nativeName' => 'Asturianu' ],
        'ar'         => [ 'name' => 'Arabic',                    'nativeName' => 'عربي', 'orientation' => 'rtl'],
        'as'         => [ 'name' => 'Assamese',                  'nativeName' => 'অসমীয়া' ],
        'be'         => [ 'name' => 'Belarusian',                'nativeName' => 'Беларуская' ],
        'bg'         => [ 'name' => 'Bulgarian',                 'nativeName' => 'Български' ],
        'bn'         => [ 'name' => 'Bengali',                   'nativeName' => 'বাংলা' ],
        'bn-BD'      => [ 'name' => 'Bengali (Bangladesh)',      'nativeName' => 'বাংলা (বাংলাদেশ)' ],
        'bn-IN'      => [ 'name' => 'Bengali (India)',           'nativeName' => 'বাংলা (ভারত)' ],
        'br'         => [ 'name' => 'Breton',                    'nativeName' => 'Brezhoneg' ],
        'ca'         => [ 'name' => 'Catalan',                   'nativeName' => 'Català' ],
        'ca-valencia'=> [ 'name' => 'Catalan (Valencian)',       'nativeName' => 'Català (valencià)' ], // not iso-639-1. a=l10n-drivers
        'cs'         => [ 'name' => 'Czech',                     'nativeName' => 'Čeština' ],
        'cy'         => [ 'name' => 'Welsh',                     'nativeName' => 'Cymraeg' ],
        'da'         => [ 'name' => 'Danish',                    'nativeName' => 'Dansk' ],
        'de'         => [ 'name' => 'German',                    'nativeName' => 'Deutsch' ],
        'de-AT'      => [ 'name' => 'German (Austria)',          'nativeName' => 'Deutsch (Österreich)' ],
        'de-CH'      => [ 'name' => 'German (Switzerland)',      'nativeName' => 'Deutsch (Schweiz)' ],
        'de-DE'      => [ 'name' => 'German (Germany)',          'nativeName' => 'Deutsch (Deutschland)' ],
        'dsb'        => [ 'name' => 'Lower Sorbian',             'nativeName' => 'Dolnoserbšćina' ], // iso-639-2
        'el'         => [ 'name' => 'Greek',                     'nativeName' => 'Ελληνικά' ],
        'en'         => [ 'name' => 'English',                   'nativeName' => 'English' ],
        'en-AU'      => [ 'name' => 'English (Australian)',      'nativeName' => 'English (Australian)' ],
        'en-CA'      => [ 'name' => 'English (Canadian)',        'nativeName' => 'English (Canadian)' ],
        'en-GB'      => [ 'name' => 'English (British)',         'nativeName' => 'English (British)' ],
        'en-NZ'      => [ 'name' => 'English (New Zealand)',     'nativeName' => 'English (New Zealand)' ],
        'en-US'      => [ 'name' => 'English (US)',              'nativeName' => 'English (US)' ],
        'en-ZA'      => [ 'name' => 'English (South African)',   'nativeName' => 'English (South African)' ],
        'eo'         => [ 'name' => 'Esperanto',                 'nativeName' => 'Esperanto' ],
        'es'         => [ 'name' => 'Spanish',                   'nativeName' => 'Español' ],
        'es-AR'      => [ 'name' => 'Spanish (Argentina)',       'nativeName' => 'Español (de Argentina)' ],
        'es-CL'      => [ 'name' => 'Spanish (Chile)',           'nativeName' => 'Español (de Chile)' ],
        'es-ES'      => [ 'name' => 'Spanish (Spain)',           'nativeName' => 'Español (de España)' ],
        'es-MX'      => [ 'name' => 'Spanish (Mexico)',          'nativeName' => 'Español (de México)' ],
        'et'         => [ 'name' => 'Estonian',                  'nativeName' => 'Eesti keel' ],
        'eu'         => [ 'name' => 'Basque',                    'nativeName' => 'Euskara' ],
        'fa'         => [ 'name' => 'Persian',                   'nativeName' => 'فارسی' , 'orientation' => 'rtl' ],
        'fi'         => [ 'name' => 'Finnish',                   'nativeName' => 'Suomi' ],
        'fj-FJ'      => [ 'name' => 'Fijian',                    'nativeName' => 'Vosa vaka-Viti' ],
        'fr'         => [ 'name' => 'French',                    'nativeName' => 'Français' ],
        'fur'        => [ 'name' => 'Friulian',                  'nativeName' => 'Furlan' ],
        'fur-IT'     => [ 'name' => 'Friulian',                  'nativeName' => 'Furlan' ],
        'fy'         => [ 'name' => 'Frisian',                   'nativeName' => 'Frysk' ],
        'fy-NL'      => [ 'name' => 'Frisian',                   'nativeName' => 'Frysk' ],
        'ga'         => [ 'name' => 'Irish',                     'nativeName' => 'Gaeilge' ],
        'ga-IE'      => [ 'name' => 'Irish (Ireland)',           'nativeName' => 'Gaeilge (Éire)' ],
        'gd'         => [ 'name' => 'Gaelic (Scotland)',         'nativeName' => 'Gàidhlig' ],
        'gl'         => [ 'name' => 'Galician',                  'nativeName' => 'Galego' ],
        'gu'         => [ 'name' => 'Gujarati',                  'nativeName' => 'ગુજરાતી' ],
        'gu-IN'      => [ 'name' => 'Gujarati',                  'nativeName' => 'ગુજરાતી' ],
        'he'         => [ 'name' => 'Hebrew',                    'nativeName' => 'עברית', 'orientation' => 'rtl' ],
        'hi'         => [ 'name' => 'Hindi',                     'nativeName' => 'हिन्दी' ],
        'hi-IN'      => [ 'name' => 'Hindi (India)',             'nativeName' => 'हिन्दी (भारत)' ],
        'hr'         => [ 'name' => 'Croatian',                  'nativeName' => 'Hrvatski' ],
        'hsb'        => [ 'name' => 'Upper Sorbian',             'nativeName' => 'Hornjoserbsce' ],
        'hu'         => [ 'name' => 'Hungarian',                 'nativeName' => 'Magyar' ],
        'hy'         => [ 'name' => 'Armenian',                  'nativeName' => 'Հայերեն' ],
        'hy-AM'      => [ 'name' => 'Armenian',                  'nativeName' => 'Հայերեն' ],
        'id'         => [ 'name' => 'Indonesian',                'nativeName' => 'Bahasa Indonesia' ],
        'is'         => [ 'name' => 'Icelandic',                 'nativeName' => 'íslenska' ],
        'it'         => [ 'name' => 'Italian',                   'nativeName' => 'Italiano' ],
        'ja'         => [ 'name' => 'Japanese',                  'nativeName' => '日本語' ],
        'ja-JP'      => [ 'name' => 'Japanese',                  'nativeName' => '日本語' ], // not iso-639-1
        'ka'         => [ 'name' => 'Georgian',                  'nativeName' => 'ქართული' ],
        'kk'         => [ 'name' => 'Kazakh',                    'nativeName' => 'Қазақ' ],
        'kn'         => [ 'name' => 'Kannada',                   'nativeName' => 'ಕನ್ನಡ' ],
        'ko'         => [ 'name' => 'Korean',                    'nativeName' => '한국어' ],
        'ku'         => [ 'name' => 'Kurdish',                   'nativeName' => 'Kurdî' ],
        'la'         => [ 'name' => 'Latin',                     'nativeName' => 'Latina' ],
        'lb'         => [ 'name' => 'Luxembourgish',             'nativeName' => 'Lëtzebuergesch' ],
        'lg'         => [ 'name' => 'Luganda',                   'nativeName' => 'Luganda' ],
        'lt'         => [ 'name' => 'Lithuanian',                'nativeName' => 'Lietuvių kalba' ],
        'lv'         => [ 'name' => 'Latvian',                   'nativeName' => 'Latviešu' ],
        'mai'        => [ 'name' => 'Maithili',                  'nativeName' => 'मैथिली মৈথিলী' ],
        'mg'         => [ 'name' => 'Malagasy',                  'nativeName' => 'Malagasy' ],
        'mi'         => [ 'name' => 'Maori (Aotearoa)',          'nativeName' => 'Māori (Aotearoa)' ],
        'mk'         => [ 'name' => 'Macedonian',                'nativeName' => 'Македонски' ],
        'ml'         => [ 'name' => 'Malayalam',                 'nativeName' => 'മലയാളം' ],
        'mn'         => [ 'name' => 'Mongolian',                 'nativeName' => 'Монгол' ],
        'mr'         => [ 'name' => 'Marathi',                   'nativeName' => 'मराठी' ],
        'no'         => [ 'name' => 'Norwegian',                 'nativeName' => 'Norsk' ],
        'nb'         => [ 'name' => 'Norwegian',                 'nativeName' => 'Norsk' ],
        'nb-NO'      => [ 'name' => 'Norwegian (Bokmål)',        'nativeName' => 'Norsk bokmål' ],
        'ne-NP'      => [ 'name' => 'Nepali',                    'nativeName' => 'नेपाली' ],
        'nn-NO'      => [ 'name' => 'Norwegian (Nynorsk)',       'nativeName' => 'Norsk nynorsk' ],
        'nl'         => [ 'name' => 'Dutch',                     'nativeName' => 'Nederlands' ],
        'nr'         => [ 'name' => 'Ndebele, South',            'nativeName' => 'IsiNdebele' ],
        'nso'        => [ 'name' => 'Northern Sotho',            'nativeName' => 'Sepedi' ],
        'oc'         => [ 'name' => 'Occitan (Lengadocian)',     'nativeName' => 'Occitan (lengadocian)' ],
        'or'         => [ 'name' => 'Oriya',                     'nativeName' => 'ଓଡ଼ିଆ' ],
        'pa'         => [ 'name' => 'Punjabi',                   'nativeName' => 'ਪੰਜਾਬੀ' ],
        'pa-IN'      => [ 'name' => 'Punjabi',                   'nativeName' => 'ਪੰਜਾਬੀ' ],
        'pl'         => [ 'name' => 'Polish',                    'nativeName' => 'Polski' ],
        'pt'         => [ 'name' => 'Portuguese',                'nativeName' => 'Português' ],
        'pt-BR'      => [ 'name' => 'Portuguese (Brazilian)',    'nativeName' => 'Português (do Brasil)' ],
        'pt-PT'      => [ 'name' => 'Portuguese (Portugal)',     'nativeName' => 'Português (Europeu)' ],
        'ro'         => [ 'name' => 'Romanian',                  'nativeName' => 'Română' ],
        'rm'         => [ 'name' => 'Romansh',                   'nativeName' => 'Rumantsch' ],
        'ru'         => [ 'name' => 'Russian',                   'nativeName' => 'Русский' ],
        'rw'         => [ 'name' => 'Kinyarwanda',               'nativeName' => 'Ikinyarwanda' ],
        'si'         => [ 'name' => 'Sinhala',                   'nativeName' => 'සිංහල' ],
        'sk'         => [ 'name' => 'Slovak',                    'nativeName' => 'Slovenčina' ],
        'sl'         => [ 'name' => 'Slovenian',                 'nativeName' => 'Slovensko' ],
        'son'        => [ 'name' => 'Songhai',                   'nativeName' => 'Soŋay' ],
        'sq'         => [ 'name' => 'Albanian',                  'nativeName' => 'Shqip' ],
        'sr'         => [ 'name' => 'Serbian',                   'nativeName' => 'Српски' ],
        'sr-Latn'    => [ 'name' => 'Serbian',                   'nativeName' => 'Srpski' ], // follows RFC 4646
        'ss'         => [ 'name' => 'Siswati',                   'nativeName' => 'siSwati' ],
        'st'         => [ 'name' => 'Southern Sotho',            'nativeName' => 'Sesotho' ],
        'sv'         => [ 'name' => 'Swedish',                   'nativeName' => 'Svenska' ],
        'sv-SE'      => [ 'name' => 'Swedish',                   'nativeName' => 'Svenska' ],
        'ta'         => [ 'name' => 'Tamil',                     'nativeName' => 'தமிழ்' ],
        'ta-IN'      => [ 'name' => 'Tamil (India)',             'nativeName' => 'தமிழ் (இந்தியா)' ],
        'ta-LK'      => [ 'name' => 'Tamil (Sri Lanka)',         'nativeName' => 'தமிழ் (இலங்கை)' ],
        'te'         => [ 'name' => 'Telugu',                    'nativeName' => 'తెలుగు' ],
        'th'         => [ 'name' => 'Thai',                      'nativeName' => 'ไทย' ],
        'tlh'        => [ 'name' => 'Klingon',                   'nativeName' => 'Klingon' ],
        'tn'         => [ 'name' => 'Tswana',                    'nativeName' => 'Setswana' ],
        'tr'         => [ 'name' => 'Turkish',                   'nativeName' => 'Türkçe' ],
        'ts'         => [ 'name' => 'Tsonga',                    'nativeName' => 'Xitsonga' ],
        'tt'         => [ 'name' => 'Tatar',                     'nativeName' => 'Tatarça' ],
        'tt-RU'      => [ 'name' => 'Tatar',                     'nativeName' => 'Tatarça' ],
        'uk'         => [ 'name' => 'Ukrainian',                 'nativeName' => 'Українська' ],
        'ur'         => [ 'name' => 'Urdu',                      'nativeName' => 'اُردو', 'orientation' => 'rtl'  ],
        've'         => [ 'name' => 'Venda',                     'nativeName' => 'Tshivenḓa' ],
        'vi'         => [ 'name' => 'Vietnamese',                'nativeName' => 'Tiếng Việt' ],
        'wo'         => [ 'name' => 'Wolof',                     'nativeName' => 'Wolof' ],
        'xh'         => [ 'name' => 'Xhosa',                     'nativeName' => 'isiXhosa' ],
        'zh'         => [ 'name' => 'Chinese (Simplified)',      'nativeName' => '中文 (简体)' ],
        'zh-CN'      => [ 'name' => 'Chinese (Simplified)',      'nativeName' => '中文 (简体)' ],
        'zh-TW'      => [ 'name' => 'Chinese (Traditional)',     'nativeName' => '正體中文 (繁體)' ],
        'zu'         => [ 'name' => 'Zulu',                      'nativeName' => 'isiZulu' ]
>>>>>>> update grav cms
    ];

    public static function getName($code)
    {
        return static::get($code, 'name');
    }

    public static function getNativeName($code)
    {
<<<<<<< HEAD
        if (strlen($code) == 2) {
            return static::get($code, 'nativeName');
        } else {
            return static::get(substr($code, 0, 2), 'nativeName') . ' (' . substr($code, -2) . ')';
        }
=======
        if (isset(static::$codes[$code])) {
            return static::get($code, 'nativeName');
        } else {
            if (preg_match('/[a-zA-Z]{2}-[a-zA-Z]{2}/', $code)) {
                return static::get(substr($code, 0, 2), 'nativeName') . ' (' . substr($code, -2) . ')';
            } else {
                return $code;
            }

        }
    }

    public static function getOrientation($code)
    {
        if (isset(static::$codes[$code])) {
            if (isset(static::$codes[$code]['orientation'])) {
                return static::get($code, 'orientation');
            }
        }
        return 'ltr';
    }

    public static function isRtl($code)
    {
        if (static::getOrientation($code) == 'rtl') {
            return true;
        }
        return false;
>>>>>>> update grav cms
    }

    public static function getNames(array $keys)
    {
        $results = [];
        foreach ($keys as $key) {
            if (isset(static::$codes[$key])) {
                $results[$key] = static::$codes[$key];
            }

        }
        return $results;
    }

    protected static function get($code, $type)
    {
        if (isset(static::$codes[$code][$type])) {
            return static::$codes[$code][$type];
        } else {
            return false;
        }
    }
}
