<?php
<<<<<<< HEAD
namespace Grav\Common\Page\Medium;

/**
 * Renderable Medium objects can be rendered to HTML markup and Parsedown objects
 *
 * @author Grav
 * @license MIT
 *
 */
=======
/**
 * @package    Grav.Common.Page
 *
 * @copyright  Copyright (C) 2014 - 2016 RocketTheme, LLC. All rights reserved.
 * @license    MIT License; see LICENSE file for details.
 */

namespace Grav\Common\Page\Medium;

>>>>>>> update grav cms
interface RenderableInterface
{
    /**
     * Return HTML markup from the medium.
     *
     * @param string $title
     * @param string $alt
     * @param string $class
     * @param bool $reset
     * @return string
     */
    public function html($title = null, $alt = null, $class = null, $reset = true);

    /**
     * Return Parsedown Element from the medium.
     *
     * @param string $title
     * @param string $alt
     * @param string $class
<<<<<<< HEAD
     * @param bool $reset
     * @return string
     */
    public function parsedownElement($title = null, $alt = null, $class = null, $reset = true);
=======
     * @param string $id
     * @param bool $reset
     * @return string
     */
    public function parsedownElement($title = null, $alt = null, $class = null, $id = null, $reset = true);
>>>>>>> update grav cms
}
