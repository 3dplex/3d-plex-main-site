<?php
<<<<<<< HEAD
namespace Grav\Common\Page\Medium;

/**
 * The Image medium holds information related to an individual image. These are then stored in the Media object.
 *
 * @author Grav
 * @license MIT
 *
 */
=======
/**
 * @package    Grav.Common.Page
 *
 * @copyright  Copyright (C) 2014 - 2016 RocketTheme, LLC. All rights reserved.
 * @license    MIT License; see LICENSE file for details.
 */

namespace Grav\Common\Page\Medium;

>>>>>>> update grav cms
class StaticImageMedium extends Medium
{
    use StaticResizeTrait;

    /**
     * Parsedown element for source display mode
     *
     * @param  array $attributes
     * @param  boolean $reset
     * @return array
     */
    protected function sourceParsedownElement(array $attributes, $reset = true)
    {
        empty($attributes['src']) && $attributes['src'] = $this->url($reset);

<<<<<<< HEAD
        return [ 'name' => 'image', 'attributes' => $attributes ];
=======
        return [ 'name' => 'img', 'attributes' => $attributes ];
>>>>>>> update grav cms
    }
}
