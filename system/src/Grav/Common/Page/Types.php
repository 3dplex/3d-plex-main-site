<?php
<<<<<<< HEAD
namespace Grav\Common\Page;

use Grav\Common\Filesystem\Folder;
=======
/**
 * @package    Grav.Common.Page
 *
 * @copyright  Copyright (C) 2014 - 2016 RocketTheme, LLC. All rights reserved.
 * @license    MIT License; see LICENSE file for details.
 */

namespace Grav\Common\Page;

use Grav\Common\Filesystem\Folder;
use Grav\Common\Grav;
>>>>>>> update grav cms
use RocketTheme\Toolbox\ArrayTraits\ArrayAccess;
use RocketTheme\Toolbox\ArrayTraits\Constructor;
use RocketTheme\Toolbox\ArrayTraits\Countable;
use RocketTheme\Toolbox\ArrayTraits\Export;
use RocketTheme\Toolbox\ArrayTraits\Iterator;
<<<<<<< HEAD
=======
use RocketTheme\Toolbox\ResourceLocator\UniformResourceLocator;
>>>>>>> update grav cms

class Types implements \ArrayAccess, \Iterator, \Countable
{
    use ArrayAccess, Constructor, Iterator, Countable, Export;

    protected $items;
    protected $systemBlueprints;

    public function register($type, $blueprint = null)
    {
<<<<<<< HEAD
        if (!$blueprint && $this->systemBlueprints && isset($this->systemBlueprints[$type])) {
            $useBlueprint = $this->systemBlueprints[$type];
        } else {
            $useBlueprint = $blueprint;
        }

        if ($blueprint || empty($this->items[$type])) {
            $this->items[$type] = $useBlueprint;
        }
    }

    public function scanBlueprints($paths)
    {
        $this->items = $this->findBlueprints($paths) + $this->items;
    }

    public function scanTemplates($paths)
    {
=======
        if (!isset($this->items[$type])) {
            $this->items[$type] = [];
        } elseif (!$blueprint) {
            return;
        }

        if (!$blueprint && $this->systemBlueprints) {
            $blueprint = isset($this->systemBlueprints[$type]) ? $this->systemBlueprints[$type] : $this->systemBlueprints['default'];
        }

        if ($blueprint) {
            array_unshift($this->items[$type], $blueprint);
        }
    }

    public function scanBlueprints($uri)
    {
        if (!is_string($uri)) {
            throw new \InvalidArgumentException('First parameter must be URI');
        }

        if (!$this->systemBlueprints) {
            $this->systemBlueprints = $this->findBlueprints('blueprints://pages');

            // Register default by default.
            $this->register('default');

            $this->register('external');
        }

        foreach ($this->findBlueprints($uri) as $type => $blueprint) {
            $this->register($type, $blueprint);
        }
    }

    public function scanTemplates($uri)
    {
        if (!is_string($uri)) {
            throw new \InvalidArgumentException('First parameter must be URI');
        }

>>>>>>> update grav cms
        $options = [
            'compare' => 'Filename',
            'pattern' => '|\.html\.twig$|',
            'filters' => [
                'value' => '|\.html\.twig$|'
            ],
            'value' => 'Filename',
            'recursive' => false
        ];

<<<<<<< HEAD
        if (!$this->systemBlueprints) {
            $this->systemBlueprints = $this->findBlueprints('blueprints://pages');
        }

        // register default by default
        $this->register('default');

        foreach ((array) $paths as $path) {
            foreach (Folder::all($path, $options) as $type) {
                $this->register($type);
            }
            $modular_path = rtrim($path, '/') . '/modular';
            if (file_exists($modular_path)) {
                foreach (Folder::all($modular_path, $options) as $type) {
                    $this->register('modular/' . $type);
                }
=======
        foreach (Folder::all($uri, $options) as $type) {
            $this->register($type);
        }

        $modular_uri = rtrim($uri, '/') . '/modular';
        if (is_dir($modular_uri)) {
            foreach (Folder::all($modular_uri, $options) as $type) {
                $this->register('modular/' . $type);
>>>>>>> update grav cms
            }
        }
    }

    public function pageSelect()
    {
        $list = [];
        foreach ($this->items as $name => $file) {
            if (strpos($name, '/')) {
                continue;
            }
            $list[$name] = ucfirst(strtr($name, '_', ' '));
        }
        ksort($list);
        return $list;
    }

    public function modularSelect()
    {
        $list = [];
        foreach ($this->items as $name => $file) {
            if (strpos($name, 'modular/') !== 0) {
                continue;
            }
            $list[$name] = trim(ucfirst(strtr(basename($name), '_', ' ')));
        }
        ksort($list);
        return $list;
    }

<<<<<<< HEAD
    private function findBlueprints($paths)
=======
    private function findBlueprints($uri)
>>>>>>> update grav cms
    {
        $options = [
            'compare' => 'Filename',
            'pattern' => '|\.yaml$|',
            'filters' => [
                'key' => '|\.yaml$|'
                ],
            'key' => 'SubPathName',
            'value' => 'PathName',
        ];

<<<<<<< HEAD
        $list = [];
        foreach ((array) $paths as $path) {
            $list += Folder::all($path, $options);
        }

=======
        /** @var UniformResourceLocator $locator */
        $locator = Grav::instance()['locator'];
        if ($locator->isStream($uri)) {
            $options['value'] = 'Url';
        }

        $list = Folder::all($uri, $options);

>>>>>>> update grav cms
        return $list;
    }
}
