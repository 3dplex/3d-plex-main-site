<?php
<<<<<<< HEAD
=======
/**
 * @package    Grav.Common
 *
 * @copyright  Copyright (C) 2014 - 2016 RocketTheme, LLC. All rights reserved.
 * @license    MIT License; see LICENSE file for details.
 */

>>>>>>> update grav cms
namespace Grav\Common;

use Grav\Common\Config\Config;
use RocketTheme\Toolbox\File\YamlFile;

<<<<<<< HEAD
/**
 * Class Theme
 * @package Grav\Common
 */
class Theme extends Plugin
{
    public $name;

=======
class Theme extends Plugin
{
>>>>>>> update grav cms
    /**
     * Constructor.
     *
     * @param Grav   $grav
     * @param Config $config
     * @param string $name
     */
    public function __construct(Grav $grav, Config $config, $name)
    {
<<<<<<< HEAD
        $this->name = $name;

=======
>>>>>>> update grav cms
        parent::__construct($name, $grav, $config);
    }

    /**
<<<<<<< HEAD
=======
     * Get configuration of the plugin.
     *
     * @return Config
     */
    public function config()
    {
        return $this->config["themes.{$this->name}"];
    }

    /**
>>>>>>> update grav cms
     * Persists to disk the theme parameters currently stored in the Grav Config object
     *
     * @param string $theme_name The name of the theme whose config it should store.
     *
     * @return true
     */
    public static function saveConfig($theme_name)
    {
        if (!$theme_name) {
            return false;
        }

<<<<<<< HEAD
        $locator = Grav::instance()['locator'];
        $filename = 'config://themes/' . $theme_name . '.yaml';
        $file = YamlFile::instance($locator->findResource($filename, true, true));
        $content = Grav::instance()['config']->get('themes.' . $theme_name);
=======
        $grav = Grav::instance();
        $locator = $grav['locator'];
        $filename = 'config://themes/' . $theme_name . '.yaml';
        $file = YamlFile::instance($locator->findResource($filename, true, true));
        $content = $grav['config']->get('themes.' . $theme_name);
>>>>>>> update grav cms
        $file->save($content);
        $file->free();

        return true;
    }
<<<<<<< HEAD
=======

    /**
     * Simpler getter for the theme blueprint
     *
     * @return mixed
     */
    public function getBlueprint()
    {
        if (!$this->blueprint) {
            $this->loadBlueprint();
        }
        return $this->blueprint;
    }

    /**
     * Load blueprints.
     */
    protected function loadBlueprint()
    {
        if (!$this->blueprint) {
            $grav = Grav::instance();
            $themes = $grav['themes'];
            $this->blueprint = $themes->get($this->name)->blueprints();
        }
    }
>>>>>>> update grav cms
}
