<?php
<<<<<<< HEAD
=======
/**
 * @package    Grav.Common.User
 *
 * @copyright  Copyright (C) 2014 - 2016 RocketTheme, LLC. All rights reserved.
 * @license    MIT License; see LICENSE file for details.
 */

>>>>>>> update grav cms
namespace Grav\Common\User;

use Grav\Common\Data\Blueprints;
use Grav\Common\Data\Data;
use Grav\Common\File\CompiledYamlFile;
<<<<<<< HEAD
use Grav\Common\GravTrait;
use Grav\Common\Utils;

/**
 * Group object
 *
 * @author  RocketTheme
 * @license MIT
 */
class Group extends Data
{
    use GravTrait;

=======
use Grav\Common\Grav;
use Grav\Common\Utils;

class Group extends Data
{
>>>>>>> update grav cms
    /**
     * Get the groups list
     *
     * @return array
     */
    private static function groups()
    {
<<<<<<< HEAD
        $groups = self::getGrav()['config']->get('groups');
=======
        $groups = Grav::instance()['config']->get('groups');
>>>>>>> update grav cms

        return $groups;
    }

    /**
     * Checks if a group exists
     *
     * @param string $groupname
     *
     * @return object
     */
    public static function groupExists($groupname)
    {
        return isset(self::groups()[$groupname]);
    }

    /**
     * Get a group by name
     *
     * @param string $groupname
     *
     * @return object
     */
    public static function load($groupname)
    {
        if (self::groupExists($groupname)) {
            $content = self::groups()[$groupname];
        } else {
            $content = [];
        }

<<<<<<< HEAD
        $blueprints = new Blueprints('blueprints://');
=======
        $blueprints = new Blueprints;
>>>>>>> update grav cms
        $blueprint = $blueprints->get('user/group');
        if (!isset($content['groupname'])) {
            $content['groupname'] = $groupname;
        }
        $group = new Group($content, $blueprint);

        return $group;
    }

    /**
     * Save a group
     */
    public function save()
    {
<<<<<<< HEAD
        $blueprints = new Blueprints('blueprints://');
=======
        $grav = Grav::instance();
        $config = $grav['config'];

        $blueprints = new Blueprints;
>>>>>>> update grav cms
        $blueprint = $blueprints->get('user/group');

        $fields = $blueprint->fields();

<<<<<<< HEAD
        self::getGrav()['config']->set("groups.$this->groupname", []);
=======
        $config->set("groups.$this->groupname", []);
>>>>>>> update grav cms

        foreach ($fields as $field) {
            if ($field['type'] == 'text') {
                $value = $field['name'];
<<<<<<< HEAD
                if (isset($this->items[$value])) {
                    self::getGrav()['config']->set("groups.$this->groupname.$value", $this->items[$value]);
=======
                if (isset($this->items['data'][$value])) {
                    $config->set("groups.$this->groupname.$value", $this->items['data'][$value]);
>>>>>>> update grav cms
                }
            }
            if ($field['type'] == 'array') {
                $value = $field['name'];
<<<<<<< HEAD
                $arrayValues = Utils::resolve($this->items, $field['name']);

                if ($arrayValues) {
                    foreach ($arrayValues as $arrayIndex => $arrayValue) {
                        self::getGrav()['config']->set("groups.$this->groupname.$value.$arrayIndex", $arrayValue);
=======
                $arrayValues = Utils::getDotNotation($this->items['data'], $field['name']);

                if ($arrayValues) {
                    foreach ($arrayValues as $arrayIndex => $arrayValue) {
                        $config->set("groups.$this->groupname.$value.$arrayIndex", $arrayValue);
>>>>>>> update grav cms
                    }
                }
            }
        }

        $type = 'groups';
        $blueprints = $this->blueprints("config/{$type}");
<<<<<<< HEAD
        $obj = new Data(self::getGrav()['config']->get($type), $blueprints);
        $file = CompiledYamlFile::instance(self::getGrav()['locator']->findResource("config://{$type}.yaml"));
=======
        $obj = new Data($config->get($type), $blueprints);
        $file = CompiledYamlFile::instance($grav['locator']->findResource("config://{$type}.yaml"));
>>>>>>> update grav cms
        $obj->file($file);
        $obj->save();
    }

    /**
     * Remove a group
     *
     * @param string $groupname
     *
     * @return bool True if the action was performed
     */
    public static function remove($groupname)
    {
<<<<<<< HEAD
        $blueprints = new Blueprints('blueprints://');
        $blueprint = $blueprints->get('user/group');

        $groups = self::getGrav()['config']->get("groups");
        unset($groups[$groupname]);
        self::getGrav()['config']->set("groups", $groups);

        $type = 'groups';
        $obj = new Data(self::getGrav()['config']->get($type), $blueprint);
        $file = CompiledYamlFile::instance(self::getGrav()['locator']->findResource("config://{$type}.yaml"));
=======
        $grav = Grav::instance();
        $config = $grav['config'];
        $blueprints = new Blueprints;
        $blueprint = $blueprints->get('user/group');

        $groups = $config->get("groups");
        unset($groups[$groupname]);
        $config->set("groups", $groups);

        $type = 'groups';
        $obj = new Data($config->get($type), $blueprint);
        $file = CompiledYamlFile::instance($grav['locator']->findResource("config://{$type}.yaml"));
>>>>>>> update grav cms
        $obj->file($file);
        $obj->save();

        return true;
    }
}
