<?php
<<<<<<< HEAD
=======
/**
 * @package    Grav.Console
 *
 * @copyright  Copyright (C) 2014 - 2016 RocketTheme, LLC. All rights reserved.
 * @license    MIT License; see LICENSE file for details.
 */

>>>>>>> update grav cms
namespace Grav\Console\Gpm;

use Grav\Common\GPM\GPM;
use Grav\Common\GPM\Installer;
use Grav\Console\ConsoleCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;

<<<<<<< HEAD
/**
 * Class UpdateCommand
 * @package Grav\Console\Gpm
 */
=======
>>>>>>> update grav cms
class UpdateCommand extends ConsoleCommand
{
    /**
     * @var
     */
    protected $data;
    /**
     * @var
     */
    protected $extensions;
    /**
     * @var
     */
    protected $updatable;
    /**
     * @var
     */
    protected $destination;
    /**
     * @var
     */
    protected $file;
    /**
     * @var array
     */
<<<<<<< HEAD
    protected $types = array('plugins', 'themes');
=======
    protected $types = ['plugins', 'themes'];
>>>>>>> update grav cms
    /**
     * @var GPM $gpm
     */
    protected $gpm;

<<<<<<< HEAD
=======
    protected $all_yes;

    protected $overwrite;

>>>>>>> update grav cms
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName("update")
            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force re-fetching the data from remote'
            )
            ->addOption(
                'destination',
                'd',
                InputOption::VALUE_OPTIONAL,
                'The grav instance location where the updates should be applied to. By default this would be where the grav cli has been launched from',
                GRAV_ROOT
            )
            ->addOption(
                'all-yes',
                'y',
                InputOption::VALUE_NONE,
                'Assumes yes (or best approach) instead of prompting'
            )
<<<<<<< HEAD
=======
            ->addOption(
                'overwrite',
                'o',
                InputOption::VALUE_NONE,
                'Option to overwrite packages if they already exist'
            )
            ->addOption(
                'plugins',
                'p',
                InputOption::VALUE_NONE,
                'Update only plugins'
            )
            ->addOption(
                'themes',
                't',
                InputOption::VALUE_NONE,
                'Update only themes'
            )
>>>>>>> update grav cms
            ->addArgument(
                'package',
                InputArgument::IS_ARRAY | InputArgument::OPTIONAL,
                'The package or packages that is desired to update. By default all available updates will be applied.'
            )
            ->setDescription("Detects and performs an update of plugins and themes when available")
            ->setHelp('The <info>update</info> command updates plugins and themes when a new version is available');
    }

    /**
     * @return int|null|void
     */
    protected function serve()
    {
        $this->gpm = new GPM($this->input->getOption('force'));
<<<<<<< HEAD
        $this->destination = realpath($this->input->getOption('destination'));
        $skip_prompt = $this->input->getOption('all-yes');
=======

        $this->all_yes = $this->input->getOption('all-yes');
        $this->overwrite = $this->input->getOption('overwrite');

        $this->displayGPMRelease();

        $this->destination = realpath($this->input->getOption('destination'));
>>>>>>> update grav cms

        if (!Installer::isGravInstance($this->destination)) {
            $this->output->writeln("<red>ERROR</red>: " . Installer::lastErrorMsg());
            exit;
        }
<<<<<<< HEAD

        $this->data = $this->gpm->getUpdatable();
        $only_packages = array_map('strtolower', $this->input->getArgument('package'));

        if (!$this->data['total']) {
=======
        if ($this->input->getOption('plugins') === false && $this->input->getOption('themes') === false) {
            $list_type = ['plugins' => true, 'themes' => true];
        } else {
            $list_type['plugins'] = $this->input->getOption('plugins');
            $list_type['themes'] = $this->input->getOption('themes');
        }

        if ($this->overwrite) {
            $this->data = $this->gpm->getInstallable($list_type);
            $description = " can be overwritten";
        } else {
            $this->data = $this->gpm->getUpdatable($list_type);
            $description = " need updating";
        }

        $only_packages = array_map('strtolower', $this->input->getArgument('package'));

        if (!$this->overwrite && !$this->data['total']) {
>>>>>>> update grav cms
            $this->output->writeln("Nothing to update.");
            exit;
        }

<<<<<<< HEAD
        $this->output->write("Found <green>" . $this->gpm->countInstalled() . "</green> extensions installed of which <magenta>" . $this->data['total'] . "</magenta> need updating");
=======
        $this->output->write("Found <green>" . $this->gpm->countInstalled() . "</green> packages installed of which <magenta>" . $this->data['total'] . "</magenta>" . $description);
>>>>>>> update grav cms

        $limit_to = $this->userInputPackages($only_packages);

        $this->output->writeln('');

        unset($this->data['total']);
        unset($limit_to['total']);


        // updates review
        $slugs = [];

        $index = 0;
        foreach ($this->data as $packages) {
            foreach ($packages as $slug => $package) {
                if (count($limit_to) && !array_key_exists($slug, $limit_to)) {
                    continue;
                }

<<<<<<< HEAD
=======
                if (!$package->available) {
                    $package->available = $package->version;
                }

>>>>>>> update grav cms
                $this->output->writeln(
                // index
                    str_pad($index++ + 1, 2, '0', STR_PAD_LEFT) . ". " .
                    // name
                    "<cyan>" . str_pad($package->name, 15) . "</cyan> " .
                    // version
<<<<<<< HEAD
                    "[v<magenta>" . $package->version . "</magenta> ➜ v<green>" . $package->available . "</green>]"
=======
                    "[v<magenta>" . $package->version . "</magenta> -> v<green>" . $package->available . "</green>]"
>>>>>>> update grav cms
                );
                $slugs[] = $slug;
            }
        }

<<<<<<< HEAD
        if (!$skip_prompt) {
=======
        if (!$this->all_yes) {
>>>>>>> update grav cms
            // prompt to continue
            $this->output->writeln("");
            $questionHelper = $this->getHelper('question');
            $question = new ConfirmationQuestion("Continue with the update process? [Y|n] ", true);
            $answer = $questionHelper->ask($this->input, $this->output, $question);

            if (!$answer) {
<<<<<<< HEAD
                $this->output->writeln("Update aborted. Exiting...");
=======
                $this->output->writeln("<red>Update aborted. Exiting...</red>");
>>>>>>> update grav cms
                exit;
            }
        }

        // finally update
        $install_command = $this->getApplication()->find('install');

<<<<<<< HEAD
        $args = new ArrayInput(array(
            'command' => 'install',
            'package' => $slugs,
            '-f'      => $this->input->getOption('force'),
            '-d'      => $this->destination,
            '-y'      => true
        ));
        $command_exec = $install_command->run($args, $this->output);

        if ($command_exec != 0) {
            $this->output->writeln("<red>Error:</red> An error occurred while trying to install the extensions");
=======
        $args = new ArrayInput([
            'command' => 'install',
            'package' => $slugs,
            '-f' => $this->input->getOption('force'),
            '-d' => $this->destination,
            '-y' => true
        ]);
        $command_exec = $install_command->run($args, $this->output);

        if ($command_exec != 0) {
            $this->output->writeln("<red>Error:</red> An error occurred while trying to install the packages");
>>>>>>> update grav cms
            exit;
        }
    }

    /**
     * @param $only_packages
     *
     * @return array
     */
    private function userInputPackages($only_packages)
    {
        $found = ['total' => 0];
        $ignore = [];

        if (!count($only_packages)) {
            $this->output->writeln('');
        } else {
            foreach ($only_packages as $only_package) {
                $find = $this->gpm->findPackage($only_package);

<<<<<<< HEAD
                if (!$find || !$this->gpm->isUpdatable($find->slug)) {
=======
                if (!$find || (!$this->overwrite && !$this->gpm->isUpdatable($find->slug))) {
>>>>>>> update grav cms
                    $name = isset($find->slug) ? $find->slug : $only_package;
                    $ignore[$name] = $name;
                } else {
                    $found[$find->slug] = $find;
                    $found['total']++;
                }
            }

            if ($found['total']) {
                $list = $found;
                unset($list['total']);
                $list = array_keys($list);

                if ($found['total'] !== $this->data['total']) {
                    $this->output->write(", only <magenta>" . $found['total'] . "</magenta> will be updated");
                }

                $this->output->writeln('');
                $this->output->writeln("Limiting updates for only <cyan>" . implode('</cyan>, <cyan>',
                        $list) . "</cyan>");
            }

            if (count($ignore)) {
<<<<<<< HEAD
=======
                $this->output->writeln('');
>>>>>>> update grav cms
                $this->output->writeln("Packages not found or not requiring updates: <red>" . implode('</red>, <red>',
                        $ignore) . "</red>");
            }
        }

        return $found;
    }
}
