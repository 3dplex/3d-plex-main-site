<?php
namespace Grav\Plugin\Admin;

use Grav\Common\Grav;
use Grav\Common\GPM\GPM as GravGPM;
<<<<<<< HEAD
=======
use Grav\Common\GPM\Licenses;
>>>>>>> update grav cms
use Grav\Common\GPM\Installer;
use Grav\Common\GPM\Response;
use Grav\Common\GPM\Upgrader;
use Grav\Common\Filesystem\Folder;
use Grav\Common\GPM\Common\Package;
<<<<<<< HEAD

class Gpm
{
    // Probably should move this to Grav DI container?
    protected static $GPM;
=======
use Grav\Plugin\Admin;

/**
 * Class Gpm
 * @package Grav\Plugin\Admin
 */
class Gpm
{
    // Probably should move this to Grav DI container?
    /** @var GravGPM */
    protected static $GPM;

>>>>>>> update grav cms
    public static function GPM()
    {
        if (!static::$GPM) {
            static::$GPM = new GravGPM();
        }
<<<<<<< HEAD
=======

>>>>>>> update grav cms
        return static::$GPM;
    }

    /**
     * Default options for the install
     * @var array
     */
    protected static $options = [
        'destination'     => GRAV_ROOT,
        'overwrite'       => true,
        'ignore_symlinks' => true,
        'skip_invalid'    => true,
        'install_deps'    => true,
        'theme'           => false
    ];

    /**
     * @param Package[]|string[]|string $packages
<<<<<<< HEAD
     * @param array $options
=======
     * @param array                     $options
     *
>>>>>>> update grav cms
     * @return bool
     */
    public static function install($packages, array $options)
    {
        $options = array_merge(self::$options, $options);

        if (
            !Installer::isGravInstance($options['destination']) ||
            !Installer::isValidDestination($options['destination'], [Installer::EXISTS, Installer::IS_LINK])
        ) {
            return false;
        }

<<<<<<< HEAD
        $packages = is_array($packages) ? $packages : [ $packages ];
=======
        $packages = is_array($packages) ? $packages : [$packages];
>>>>>>> update grav cms
        $count = count($packages);

        $packages = array_filter(array_map(function ($p) {
            return !is_string($p) ? $p instanceof Package ? $p : false : self::GPM()->findPackage($p);
        }, $packages));

        if (!$options['skip_invalid'] && $count !== count($packages)) {
            return false;
        }

<<<<<<< HEAD
=======
        $messages = '';

>>>>>>> update grav cms
        foreach ($packages as $package) {
            if (isset($package->dependencies) && $options['install_deps']) {
                $result = static::install($package->dependencies, $options);

                if (!$result) {
                    return false;
                }
            }

            // Check destination
            Installer::isValidDestination($options['destination'] . DS . $package->install_path);

            if (Installer::lastErrorCode() === Installer::EXISTS && !$options['overwrite']) {
                return false;
            }

            if (Installer::lastErrorCode() === Installer::IS_LINK && !$options['ignore_symlinks']) {
                return false;
            }

<<<<<<< HEAD
            $local = static::download($package);

            Installer::install($local, $options['destination'], ['install_path' => $package->install_path, 'theme' => $options['theme']]);
            Folder::delete(dirname($local));

            $errorCode = Installer::lastErrorCode();

            if (Installer::lastErrorCode() & (Installer::ZIP_OPEN_ERROR | Installer::ZIP_EXTRACT_ERROR)) {
                return false;
            }
        }

        return true;
=======
            $license = Licenses::get($package->slug);
            $local = static::download($package, $license);

            Installer::install($local, $options['destination'],
                ['install_path' => $package->install_path, 'theme' => $options['theme']]);
            Folder::delete(dirname($local));

            $errorCode = Installer::lastErrorCode();
            if ($errorCode) {
                $msg = Installer::lastErrorMsg();
                throw new \RuntimeException($msg);
            }

            if (count($packages) == 1) {
                $message = Installer::getMessage();
                if ($message) {
                    return $message;
                } else {
                    $messages .= $message;
                }
            }
        }

        return $messages ?: true;
>>>>>>> update grav cms
    }

    /**
     * @param Package[]|string[]|string $packages
<<<<<<< HEAD
     * @param array $options
=======
     * @param array                     $options
     *
>>>>>>> update grav cms
     * @return bool
     */
    public static function update($packages, array $options)
    {
        $options['overwrite'] = true;

        return static::install($packages, $options);
    }

    /**
     * @param Package[]|string[]|string $packages
<<<<<<< HEAD
     * @param array $options
=======
     * @param array                     $options
     *
>>>>>>> update grav cms
     * @return bool
     */
    public static function uninstall($packages, array $options)
    {
        $options = array_merge(self::$options, $options);

<<<<<<< HEAD
        $packages = is_array($packages) ? $packages : [ $packages ];
=======
        $packages = is_array($packages) ? $packages : [$packages];
>>>>>>> update grav cms
        $count = count($packages);

        $packages = array_filter(array_map(function ($p) {

            if (is_string($p)) {
                $p = strtolower($p);
                $plugin = static::GPM()->getInstalledPlugin($p);
                $p = $plugin ?: static::GPM()->getInstalledTheme($p);
            }

            return $p instanceof Package ? $p : false;

        }, $packages));

        if (!$options['skip_invalid'] && $count !== count($packages)) {
            return false;
        }

        foreach ($packages as $package) {

            $location = Grav::instance()['locator']->findResource($package->package_type . '://' . $package->slug);

            // Check destination
            Installer::isValidDestination($location);

            if (Installer::lastErrorCode() === Installer::IS_LINK && !$options['ignore_symlinks']) {
                return false;
            }

            Installer::uninstall($location);

            $errorCode = Installer::lastErrorCode();
            if ($errorCode && $errorCode !== Installer::IS_LINK && $errorCode !== Installer::EXISTS) {
<<<<<<< HEAD
                return false;
=======
                $msg = Installer::lastErrorMsg();
                throw new \RuntimeException($msg);
            }

            if (count($packages) == 1) {
                $message = Installer::getMessage();
                if ($message) {
                    return $message;
                }
>>>>>>> update grav cms
            }
        }

        return true;
    }

    /**
     * @param Package $package
<<<<<<< HEAD
     * @return string
     */
    private static function download(Package $package)
    {
        $contents = Response::get($package->zipball_url, []);

        $cache_dir = Grav::instance()['locator']->findResource('cache://', true);
        $cache_dir = $cache_dir . DS . 'tmp/Grav-' . uniqid();
        Folder::mkdir($cache_dir);

        $filename = $package->slug . basename($package->zipball_url);

        file_put_contents($cache_dir . DS . $filename . '.zip', $contents);

        return $cache_dir . DS . $filename . '.zip';
    }

    /**
     * @param array $package
     * @param string $tmp
=======
     *
     * @return string
     */
    private static function download(Package $package, $license = null)
    {
        $query = '';

        if ($package->premium) {
            $query = \json_encode(array_merge(
                $package->premium,
                [
                    'slug' => $package->slug,
                    'filename' => $package->premium['filename'],
                    'license_key' => $license
                ]
            ));

            $query = '?d=' . base64_encode($query);
        }

        try {
            $contents = Response::get($package->zipball_url . $query, []);
        } catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage());
        }

        $tmp_dir = Admin::getTempDir() . '/Grav-' . uniqid();
        Folder::mkdir($tmp_dir);

        $filename = $package->slug . basename($package->zipball_url);

        file_put_contents($tmp_dir . DS . $filename . '.zip', $contents);

        return $tmp_dir . DS . $filename . '.zip';
    }

    /**
     * @param array  $package
     * @param string $tmp
     *
>>>>>>> update grav cms
     * @return string
     */
    private static function _downloadSelfupgrade(array $package, $tmp)
    {
        $output = Response::get($package['download'], []);
        Folder::mkdir($tmp);
        file_put_contents($tmp . DS . $package['name'], $output);
<<<<<<< HEAD
=======

>>>>>>> update grav cms
        return $tmp . DS . $package['name'];
    }

    /**
     * @return bool
     */
    public static function selfupgrade()
    {
        $upgrader = new Upgrader();

        if (!Installer::isGravInstance(GRAV_ROOT)) {
            return false;
        }

        if (is_link(GRAV_ROOT . DS . 'index.php')) {
            Installer::setError(Installer::IS_LINK);
<<<<<<< HEAD
=======

>>>>>>> update grav cms
            return false;
        }

        if (method_exists($upgrader, 'meetsRequirements') && !$upgrader->meetsRequirements()) {
            $error = [];
            $error[] = '<p>Grav has increased the minimum PHP requirement.<br />';
<<<<<<< HEAD
            $error[] = 'You are currently running PHP <strong>' . PHP_VERSION .'</strong>';
            $error[] = ', but PHP <strong>' . GRAV_PHP_MIN .'</strong> is required.</p>';
            $error[] = '<p><a href="http://getgrav.org/blog/changing-php-requirements-to-5.5" class="button button-small secondary">Additional information</a></p>';

            Installer::setError(implode("\n", $error));
=======
            $error[] = 'You are currently running PHP <strong>' . PHP_VERSION . '</strong>';
            $error[] = ', but PHP <strong>' . GRAV_PHP_MIN . '</strong> is required.</p>';
            $error[] = '<p><a href="http://getgrav.org/blog/changing-php-requirements-to-5.5" class="button button-small secondary">Additional information</a></p>';

            Installer::setError(implode("\n", $error));

>>>>>>> update grav cms
            return false;
        }

        $update = $upgrader->getAssets()['grav-update'];
<<<<<<< HEAD
        $tmp = CACHE_DIR . 'tmp/Grav-' . uniqid();
=======
        $tmp = Admin::getTempDir() . '/Grav-' . uniqid();
>>>>>>> update grav cms
        $file = self::_downloadSelfupgrade($update, $tmp);

        Installer::install($file, GRAV_ROOT,
            ['sophisticated' => true, 'overwrite' => true, 'ignore_symlinks' => true]);

        $errorCode = Installer::lastErrorCode();

        Folder::delete($tmp);

        if ($errorCode & (Installer::ZIP_OPEN_ERROR | Installer::ZIP_EXTRACT_ERROR)) {
            return false;
        }

        return true;
    }
}
