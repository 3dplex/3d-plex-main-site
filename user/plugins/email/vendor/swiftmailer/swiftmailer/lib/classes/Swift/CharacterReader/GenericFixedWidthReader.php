<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2004-2009 Chris Corbyn
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Provides fixed-width byte sizes for reading fixed-width character sets.
 *
<<<<<<< HEAD
 * @package    Swift
 * @subpackage Encoder
 * @author     Chris Corbyn
 * @author     Xavier De Cock <xdecock@gmail.com>
=======
 * @author Chris Corbyn
 * @author Xavier De Cock <xdecock@gmail.com>
>>>>>>> update grav cms
 */
class Swift_CharacterReader_GenericFixedWidthReader implements Swift_CharacterReader
{
    /**
     * The number of bytes in a single character.
     *
<<<<<<< HEAD
     * @var integer
=======
     * @var int
>>>>>>> update grav cms
     */
    private $_width;

    /**
     * Creates a new GenericFixedWidthReader using $width bytes per character.
     *
<<<<<<< HEAD
     * @param integer $width
=======
     * @param int $width
>>>>>>> update grav cms
     */
    public function __construct($width)
    {
        $this->_width = $width;
    }

    /**
     * Returns the complete character map.
     *
<<<<<<< HEAD
     * @param string  $string
     * @param integer $startOffset
     * @param array   $currentMap
     * @param mixed   $ignoredChars
     *
     * @return integer
=======
     * @param string $string
     * @param int    $startOffset
     * @param array  $currentMap
     * @param mixed  $ignoredChars
     *
     * @return int
>>>>>>> update grav cms
     */
    public function getCharPositions($string, $startOffset, &$currentMap, &$ignoredChars)
    {
        $strlen = strlen($string);
        // % and / are CPU intensive, so, maybe find a better way
        $ignored = $strlen % $this->_width;
<<<<<<< HEAD
        $ignoredChars = substr($string, - $ignored);
=======
        $ignoredChars = $ignored ? substr($string, -$ignored) : '';
>>>>>>> update grav cms
        $currentMap = $this->_width;

        return ($strlen - $ignored) / $this->_width;
    }

    /**
     * Returns the mapType.
     *
<<<<<<< HEAD
     * @return integer
=======
     * @return int
>>>>>>> update grav cms
     */
    public function getMapType()
    {
        return self::MAP_TYPE_FIXED_LEN;
    }

    /**
     * Returns an integer which specifies how many more bytes to read.
     *
     * A positive integer indicates the number of more bytes to fetch before invoking
     * this method again.
     *
     * A value of zero means this is already a valid character.
     * A value of -1 means this cannot possibly be a valid character.
     *
<<<<<<< HEAD
     * @param string  $bytes
     * @param integer $size
     *
     * @return integer
=======
     * @param string $bytes
     * @param int    $size
     *
     * @return int
>>>>>>> update grav cms
     */
    public function validateByteSequence($bytes, $size)
    {
        $needed = $this->_width - $size;

<<<<<<< HEAD
        return ($needed > -1) ? $needed : -1;
=======
        return $needed > -1 ? $needed : -1;
>>>>>>> update grav cms
    }

    /**
     * Returns the number of bytes which should be read to start each character.
     *
<<<<<<< HEAD
     * @return integer
=======
     * @return int
>>>>>>> update grav cms
     */
    public function getInitialByteSize()
    {
        return $this->_width;
    }
}
