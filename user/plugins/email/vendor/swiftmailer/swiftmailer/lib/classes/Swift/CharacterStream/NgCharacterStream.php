<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2004-2009 Chris Corbyn
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * A CharacterStream implementation which stores characters in an internal array.
 *
<<<<<<< HEAD
 * @package    Swift
 * @subpackage CharacterStream
 * @author     Xavier De Cock <xdecock@gmail.com>
 */

=======
 * @author Xavier De Cock <xdecock@gmail.com>
 */
>>>>>>> update grav cms
class Swift_CharacterStream_NgCharacterStream implements Swift_CharacterStream
{
    /**
     * The char reader (lazy-loaded) for the current charset.
     *
     * @var Swift_CharacterReader
     */
    private $_charReader;

    /**
     * A factory for creating CharacterReader instances.
     *
     * @var Swift_CharacterReaderFactory
     */
    private $_charReaderFactory;

    /**
     * The character set this stream is using.
     *
     * @var string
     */
    private $_charset;

    /**
     * The data's stored as-is.
     *
     * @var string
     */
    private $_datas = '';

    /**
<<<<<<< HEAD
     * Number of bytes in the stream
     *
     * @var integer
=======
     * Number of bytes in the stream.
     *
     * @var int
>>>>>>> update grav cms
     */
    private $_datasSize = 0;

    /**
     * Map.
     *
     * @var mixed
     */
    private $_map;

    /**
     * Map Type.
     *
<<<<<<< HEAD
     * @var integer
=======
     * @var int
>>>>>>> update grav cms
     */
    private $_mapType = 0;

    /**
     * Number of characters in the stream.
     *
<<<<<<< HEAD
     * @var integer
=======
     * @var int
>>>>>>> update grav cms
     */
    private $_charCount = 0;

    /**
     * Position in the stream.
     *
<<<<<<< HEAD
     * @var integer
=======
     * @var int
>>>>>>> update grav cms
     */
    private $_currentPos = 0;

    /**
     * Constructor.
     *
     * @param Swift_CharacterReaderFactory $factory
     * @param string                       $charset
     */
    public function __construct(Swift_CharacterReaderFactory $factory, $charset)
    {
        $this->setCharacterReaderFactory($factory);
        $this->setCharacterSet($charset);
    }

    /* -- Changing parameters of the stream -- */

    /**
     * Set the character set used in this CharacterStream.
     *
     * @param string $charset
     */
    public function setCharacterSet($charset)
    {
        $this->_charset = $charset;
        $this->_charReader = null;
        $this->_mapType = 0;
    }

    /**
     * Set the CharacterReaderFactory for multi charset support.
     *
     * @param Swift_CharacterReaderFactory $factory
     */
    public function setCharacterReaderFactory(Swift_CharacterReaderFactory $factory)
    {
        $this->_charReaderFactory = $factory;
    }

    /**
     * @see Swift_CharacterStream::flushContents()
     */
    public function flushContents()
    {
        $this->_datas = null;
        $this->_map = null;
        $this->_charCount = 0;
        $this->_currentPos = 0;
        $this->_datasSize = 0;
    }

    /**
     * @see Swift_CharacterStream::importByteStream()
     *
     * @param Swift_OutputByteStream $os
     */
    public function importByteStream(Swift_OutputByteStream $os)
    {
        $this->flushContents();
<<<<<<< HEAD
        $blocks=512;
        $os->setReadPointer(0);
        while(false!==($read = $os->read($blocks)))
            $this->write($read);
=======
        $blocks = 512;
        $os->setReadPointer(0);
        while (false !== ($read = $os->read($blocks))) {
            $this->write($read);
        }
>>>>>>> update grav cms
    }

    /**
     * @see Swift_CharacterStream::importString()
     *
     * @param string $string
     */
    public function importString($string)
    {
        $this->flushContents();
        $this->write($string);
    }

    /**
     * @see Swift_CharacterStream::read()
     *
<<<<<<< HEAD
     * @param integer $length
=======
     * @param int $length
>>>>>>> update grav cms
     *
     * @return string
     */
    public function read($length)
    {
<<<<<<< HEAD
        if ($this->_currentPos>=$this->_charCount) {
          return false;
        }
        $ret=false;
        $length = ($this->_currentPos+$length > $this->_charCount)
          ? $this->_charCount - $this->_currentPos
          : $length;
          switch ($this->_mapType) {
            case Swift_CharacterReader::MAP_TYPE_FIXED_LEN:
                $len = $length*$this->_map;
=======
        if ($this->_currentPos >= $this->_charCount) {
            return false;
        }
        $ret = false;
        $length = $this->_currentPos + $length > $this->_charCount ? $this->_charCount - $this->_currentPos : $length;
        switch ($this->_mapType) {
            case Swift_CharacterReader::MAP_TYPE_FIXED_LEN:
                $len = $length * $this->_map;
>>>>>>> update grav cms
                $ret = substr($this->_datas,
                        $this->_currentPos * $this->_map,
                        $len);
                $this->_currentPos += $length;
                break;

            case Swift_CharacterReader::MAP_TYPE_INVALID:
<<<<<<< HEAD
                $end = $this->_currentPos + $length;
                $end = $end > $this->_charCount
                    ?$this->_charCount
                    :$end;
                $ret = '';
                for (; $this->_currentPos < $length; ++$this->_currentPos) {
                    if (isset ($this->_map[$this->_currentPos])) {
=======
                $ret = '';
                for (; $this->_currentPos < $length; ++$this->_currentPos) {
                    if (isset($this->_map[$this->_currentPos])) {
>>>>>>> update grav cms
                        $ret .= '?';
                    } else {
                        $ret .= $this->_datas[$this->_currentPos];
                    }
                }
                break;

            case Swift_CharacterReader::MAP_TYPE_POSITIONS:
                $end = $this->_currentPos + $length;
<<<<<<< HEAD
                $end = $end > $this->_charCount
                    ?$this->_charCount
                    :$end;
                $ret = '';
                $start = 0;
                if ($this->_currentPos>0) {
                    $start = $this->_map['p'][$this->_currentPos-1];
=======
                $end = $end > $this->_charCount ? $this->_charCount : $end;
                $ret = '';
                $start = 0;
                if ($this->_currentPos > 0) {
                    $start = $this->_map['p'][$this->_currentPos - 1];
>>>>>>> update grav cms
                }
                $to = $start;
                for (; $this->_currentPos < $end; ++$this->_currentPos) {
                    if (isset($this->_map['i'][$this->_currentPos])) {
                        $ret .= substr($this->_datas, $start, $to - $start).'?';
                        $start = $this->_map['p'][$this->_currentPos];
                    } else {
                        $to = $this->_map['p'][$this->_currentPos];
                    }
                }
                $ret .= substr($this->_datas, $start, $to - $start);
                break;
        }

        return $ret;
    }

    /**
     * @see Swift_CharacterStream::readBytes()
     *
<<<<<<< HEAD
     * @param integer $length
     *
     * @return integer[]
     */
    public function readBytes($length)
    {
        $read=$this->read($length);
        if ($read!==false) {
=======
     * @param int $length
     *
     * @return int[]
     */
    public function readBytes($length)
    {
        $read = $this->read($length);
        if ($read !== false) {
>>>>>>> update grav cms
            $ret = array_map('ord', str_split($read, 1));

            return $ret;
        }

        return false;
    }

    /**
     * @see Swift_CharacterStream::setPointer()
     *
<<<<<<< HEAD
     * @param integer $charOffset
     */
    public function setPointer($charOffset)
    {
        if ($this->_charCount<$charOffset) {
            $charOffset=$this->_charCount;
=======
     * @param int $charOffset
     */
    public function setPointer($charOffset)
    {
        if ($this->_charCount < $charOffset) {
            $charOffset = $this->_charCount;
>>>>>>> update grav cms
        }
        $this->_currentPos = $charOffset;
    }

    /**
     * @see Swift_CharacterStream::write()
     *
     * @param string $chars
     */
    public function write($chars)
    {
        if (!isset($this->_charReader)) {
            $this->_charReader = $this->_charReaderFactory->getReaderFor(
                $this->_charset);
            $this->_map = array();
            $this->_mapType = $this->_charReader->getMapType();
        }
<<<<<<< HEAD
        $ignored='';
        $this->_datas .= $chars;
        $this->_charCount += $this->_charReader->getCharPositions(substr($this->_datas, $this->_datasSize), $this->_datasSize, $this->_map, $ignored);
        if ($ignored!==false) {
            $this->_datasSize=strlen($this->_datas)-strlen($ignored);
        } else {
            $this->_datasSize=strlen($this->_datas);
=======
        $ignored = '';
        $this->_datas .= $chars;
        $this->_charCount += $this->_charReader->getCharPositions(substr($this->_datas, $this->_datasSize), $this->_datasSize, $this->_map, $ignored);
        if ($ignored !== false) {
            $this->_datasSize = strlen($this->_datas) - strlen($ignored);
        } else {
            $this->_datasSize = strlen($this->_datas);
>>>>>>> update grav cms
        }
    }
}
