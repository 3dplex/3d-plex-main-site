<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2009 Fabien Potencier <fabien.potencier@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Base class for Spools (implements time and message limits).
 *
<<<<<<< HEAD
 * @package Swift
 * @author  Fabien Potencier
=======
 * @author Fabien Potencier
>>>>>>> update grav cms
 */
abstract class Swift_ConfigurableSpool implements Swift_Spool
{
    /** The maximum number of messages to send per flush */
    private $_message_limit;

    /** The time limit per flush */
    private $_time_limit;

    /**
     * Sets the maximum number of messages to send per flush.
     *
<<<<<<< HEAD
     * @param integer $limit
=======
     * @param int $limit
>>>>>>> update grav cms
     */
    public function setMessageLimit($limit)
    {
        $this->_message_limit = (int) $limit;
    }

    /**
     * Gets the maximum number of messages to send per flush.
     *
<<<<<<< HEAD
     * @return integer The limit
=======
     * @return int The limit
>>>>>>> update grav cms
     */
    public function getMessageLimit()
    {
        return $this->_message_limit;
    }

    /**
     * Sets the time limit (in seconds) per flush.
     *
<<<<<<< HEAD
     * @param integer $limit The limit
=======
     * @param int $limit The limit
>>>>>>> update grav cms
     */
    public function setTimeLimit($limit)
    {
        $this->_time_limit = (int) $limit;
    }

    /**
     * Gets the time limit (in seconds) per flush.
     *
<<<<<<< HEAD
     * @return integer The limit
=======
     * @return int The limit
>>>>>>> update grav cms
     */
    public function getTimeLimit()
    {
        return $this->_time_limit;
    }
}
