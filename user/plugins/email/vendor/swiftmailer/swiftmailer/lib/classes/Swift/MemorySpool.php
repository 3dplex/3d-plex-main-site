<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2011 Fabien Potencier <fabien.potencier@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Stores Messages in memory.
 *
<<<<<<< HEAD
 * @package Swift
 * @author  Fabien Potencier
=======
 * @author Fabien Potencier
>>>>>>> update grav cms
 */
class Swift_MemorySpool implements Swift_Spool
{
    protected $messages = array();
<<<<<<< HEAD
=======
    private $flushRetries = 3;
>>>>>>> update grav cms

    /**
     * Tests if this Transport mechanism has started.
     *
<<<<<<< HEAD
     * @return boolean
=======
     * @return bool
>>>>>>> update grav cms
     */
    public function isStarted()
    {
        return true;
    }

    /**
     * Starts this Transport mechanism.
     */
    public function start()
    {
    }

    /**
     * Stops this Transport mechanism.
     */
    public function stop()
    {
    }

    /**
<<<<<<< HEAD
=======
     * @param int $retries
     */
    public function setFlushRetries($retries)
    {
        $this->flushRetries = $retries;
    }

    /**
>>>>>>> update grav cms
     * Stores a message in the queue.
     *
     * @param Swift_Mime_Message $message The message to store
     *
<<<<<<< HEAD
     * @return boolean Whether the operation has succeeded
     */
    public function queueMessage(Swift_Mime_Message $message)
    {
        $this->messages[] = $message;
=======
     * @return bool Whether the operation has succeeded
     */
    public function queueMessage(Swift_Mime_Message $message)
    {
        //clone the message to make sure it is not changed while in the queue
        $this->messages[] = clone $message;
>>>>>>> update grav cms

        return true;
    }

    /**
     * Sends messages using the given transport instance.
     *
     * @param Swift_Transport $transport        A transport instance
     * @param string[]        $failedRecipients An array of failures by-reference
     *
<<<<<<< HEAD
     * @return integer The number of sent emails
=======
     * @return int The number of sent emails
>>>>>>> update grav cms
     */
    public function flushQueue(Swift_Transport $transport, &$failedRecipients = null)
    {
        if (!$this->messages) {
            return 0;
        }

        if (!$transport->isStarted()) {
            $transport->start();
        }

        $count = 0;
<<<<<<< HEAD
        while ($message = array_pop($this->messages)) {
            $count += $transport->send($message, $failedRecipients);
=======
        $retries = $this->flushRetries;
        while ($retries--) {
            try {
                while ($message = array_pop($this->messages)) {
                    $count += $transport->send($message, $failedRecipients);
                }
            } catch (Swift_TransportException $exception) {
                if ($retries) {
                    // re-queue the message at the end of the queue to give a chance
                    // to the other messages to be sent, in case the failure was due to
                    // this message and not just the transport failing
                    array_unshift($this->messages, $message);

                    // wait half a second before we try again
                    usleep(500000);
                } else {
                    throw $exception;
                }
            }
>>>>>>> update grav cms
        }

        return $count;
    }
}
