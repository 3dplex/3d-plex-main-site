<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2004-2009 Chris Corbyn
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Handles binary/7/8-bit Transfer Encoding in Swift Mailer.
 *
<<<<<<< HEAD
 * @package    Swift
 * @subpackage Mime
 * @author     Chris Corbyn
=======
 * @author Chris Corbyn
>>>>>>> update grav cms
 */
class Swift_Mime_ContentEncoder_PlainContentEncoder implements Swift_Mime_ContentEncoder
{
    /**
     * The name of this encoding scheme (probably 7bit or 8bit).
     *
     * @var string
     */
    private $_name;

    /**
     * True if canonical transformations should be done.
     *
<<<<<<< HEAD
     * @var boolean
=======
     * @var bool
>>>>>>> update grav cms
     */
    private $_canonical;

    /**
     * Creates a new PlainContentEncoder with $name (probably 7bit or 8bit).
     *
<<<<<<< HEAD
     * @param string  $name
     * @param boolean $canonical If canonicalization transformation should be done.
=======
     * @param string $name
     * @param bool   $canonical If canonicalization transformation should be done.
>>>>>>> update grav cms
     */
    public function __construct($name, $canonical = false)
    {
        $this->_name = $name;
        $this->_canonical = $canonical;
    }

    /**
     * Encode a given string to produce an encoded string.
     *
<<<<<<< HEAD
     * @param string  $string
     * @param integer $firstLineOffset ignored
     * @param integer $maxLineLength   - 0 means no wrapping will occur
=======
     * @param string $string
     * @param int    $firstLineOffset ignored
     * @param int    $maxLineLength   - 0 means no wrapping will occur
>>>>>>> update grav cms
     *
     * @return string
     */
    public function encodeString($string, $firstLineOffset = 0, $maxLineLength = 0)
    {
        if ($this->_canonical) {
            $string = $this->_canonicalize($string);
        }

        return $this->_safeWordWrap($string, $maxLineLength, "\r\n");
    }

    /**
     * Encode stream $in to stream $out.
     *
     * @param Swift_OutputByteStream $os
     * @param Swift_InputByteStream  $is
<<<<<<< HEAD
     * @param integer                $firstLineOffset ignored
     * @param integer                $maxLineLength   optional, 0 means no wrapping will occur
=======
     * @param int                    $firstLineOffset ignored
     * @param int                    $maxLineLength   optional, 0 means no wrapping will occur
>>>>>>> update grav cms
     */
    public function encodeByteStream(Swift_OutputByteStream $os, Swift_InputByteStream $is, $firstLineOffset = 0, $maxLineLength = 0)
    {
        $leftOver = '';
        while (false !== $bytes = $os->read(8192)) {
<<<<<<< HEAD
            $toencode = $leftOver . $bytes;
=======
            $toencode = $leftOver.$bytes;
>>>>>>> update grav cms
            if ($this->_canonical) {
                $toencode = $this->_canonicalize($toencode);
            }
            $wrapped = $this->_safeWordWrap($toencode, $maxLineLength, "\r\n");
            $lastLinePos = strrpos($wrapped, "\r\n");
            $leftOver = substr($wrapped, $lastLinePos);
            $wrapped = substr($wrapped, 0, $lastLinePos);

            $is->write($wrapped);
        }
        if (strlen($leftOver)) {
            $is->write($leftOver);
        }
    }

    /**
     * Get the name of this encoding scheme.
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Not used.
     */
    public function charsetChanged($charset)
    {
    }

<<<<<<< HEAD
    // -- Private methods

    /**
     * A safer (but weaker) wordwrap for unicode.
     *
     * @param string  $string
     * @param integer $length
     * @param string  $le
=======
    /**
     * A safer (but weaker) wordwrap for unicode.
     *
     * @param string $string
     * @param int    $length
     * @param string $le
>>>>>>> update grav cms
     *
     * @return string
     */
    private function _safeWordwrap($string, $length = 75, $le = "\r\n")
    {
        if (0 >= $length) {
            return $string;
        }

        $originalLines = explode($le, $string);

        $lines = array();
        $lineCount = 0;

        foreach ($originalLines as $originalLine) {
            $lines[] = '';
<<<<<<< HEAD
            $currentLine =& $lines[$lineCount++];
=======
            $currentLine = &$lines[$lineCount++];
>>>>>>> update grav cms

            //$chunks = preg_split('/(?<=[\ \t,\.!\?\-&\+\/])/', $originalLine);
            $chunks = preg_split('/(?<=\s)/', $originalLine);

            foreach ($chunks as $chunk) {
                if (0 != strlen($currentLine)
<<<<<<< HEAD
                    && strlen($currentLine . $chunk) > $length)
                {
                    $lines[] = '';
                    $currentLine =& $lines[$lineCount++];
=======
                    && strlen($currentLine.$chunk) > $length) {
                    $lines[] = '';
                    $currentLine = &$lines[$lineCount++];
>>>>>>> update grav cms
                }
                $currentLine .= $chunk;
            }
        }

        return implode("\r\n", $lines);
    }

    /**
     * Canonicalize string input (fix CRLF).
     *
     * @param string $string
     *
     * @return string
     */
    private function _canonicalize($string)
    {
        return str_replace(
            array("\r\n", "\r", "\n"),
            array("\n", "\n", "\r\n"),
            $string
            );
    }
}
