<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2004-2009 Chris Corbyn
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * An abstract means of reading data.
 *
 * Classes implementing this interface may use a subsystem which requires less
 * memory than working with large strings of data.
 *
<<<<<<< HEAD
 * @package    Swift
 * @subpackage ByteStream
 * @author     Chris Corbyn
=======
 * @author Chris Corbyn
>>>>>>> update grav cms
 */
interface Swift_OutputByteStream
{
    /**
     * Reads $length bytes from the stream into a string and moves the pointer
     * through the stream by $length.
     *
     * If less bytes exist than are requested the remaining bytes are given instead.
     * If no bytes are remaining at all, boolean false is returned.
     *
<<<<<<< HEAD
     * @param integer $length
     *
     * @return string|boolean
     *
     * @throws Swift_IoException
=======
     * @param int $length
     *
     * @throws Swift_IoException
     *
     * @return string|bool
>>>>>>> update grav cms
     */
    public function read($length);

    /**
     * Move the internal read pointer to $byteOffset in the stream.
     *
<<<<<<< HEAD
     * @param integer $byteOffset
     *
     * @return boolean
     *
     * @throws Swift_IoException
=======
     * @param int $byteOffset
     *
     * @throws Swift_IoException
     *
     * @return bool
>>>>>>> update grav cms
     */
    public function setReadPointer($byteOffset);
}
