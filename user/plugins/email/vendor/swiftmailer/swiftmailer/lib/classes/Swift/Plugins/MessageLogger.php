<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2011 Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Stores all sent emails for further usage.
 *
<<<<<<< HEAD
 * @package    Swift
 * @subpackage Plugins
 * @author     Fabien Potencier
=======
 * @author Fabien Potencier
>>>>>>> update grav cms
 */
class Swift_Plugins_MessageLogger implements Swift_Events_SendListener
{
    /**
     * @var array
     */
    private $messages;

    public function __construct()
    {
        $this->messages = array();
    }

    /**
<<<<<<< HEAD
     * Get the message list
=======
     * Get the message list.
>>>>>>> update grav cms
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
<<<<<<< HEAD
     * Get the message count
     *
     * @return integer count
=======
     * Get the message count.
     *
     * @return int count
>>>>>>> update grav cms
     */
    public function countMessages()
    {
        return count($this->messages);
    }

    /**
<<<<<<< HEAD
     * Empty the message list
     *
=======
     * Empty the message list.
>>>>>>> update grav cms
     */
    public function clear()
    {
        $this->messages = array();
    }

    /**
     * Invoked immediately before the Message is sent.
     *
     * @param Swift_Events_SendEvent $evt
     */
    public function beforeSendPerformed(Swift_Events_SendEvent $evt)
    {
        $this->messages[] = clone $evt->getMessage();
    }

    /**
     * Invoked immediately after the Message is sent.
     *
     * @param Swift_Events_SendEvent $evt
     */
    public function sendPerformed(Swift_Events_SendEvent $evt)
    {
    }
}
