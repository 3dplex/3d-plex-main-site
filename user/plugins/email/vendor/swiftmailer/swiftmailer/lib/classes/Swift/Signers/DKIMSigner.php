<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2004-2009 Chris Corbyn
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
<<<<<<< HEAD
 * DKIM Signer used to apply DKIM Signature to a message
 *
 * @package    Swift
 * @subpackage Signatures
 * @author     Xavier De Cock <xdecock@gmail.com>
=======
 * DKIM Signer used to apply DKIM Signature to a message.
 *
 * @author Xavier De Cock <xdecock@gmail.com>
>>>>>>> update grav cms
 */
class Swift_Signers_DKIMSigner implements Swift_Signers_HeaderSigner
{
    /**
<<<<<<< HEAD
     * PrivateKey
=======
     * PrivateKey.
>>>>>>> update grav cms
     *
     * @var string
     */
    protected $_privateKey;

    /**
<<<<<<< HEAD
     * DomainName
=======
     * DomainName.
>>>>>>> update grav cms
     *
     * @var string
     */
    protected $_domainName;

    /**
<<<<<<< HEAD
     * Selector
=======
     * Selector.
>>>>>>> update grav cms
     *
     * @var string
     */
    protected $_selector;

    /**
<<<<<<< HEAD
     * Hash algorithm used
=======
     * Hash algorithm used.
>>>>>>> update grav cms
     *
     * @var string
     */
    protected $_hashAlgorithm = 'rsa-sha1';

    /**
<<<<<<< HEAD
     * Body canon method
=======
     * Body canon method.
>>>>>>> update grav cms
     *
     * @var string
     */
    protected $_bodyCanon = 'simple';

    /**
<<<<<<< HEAD
     * Header canon method
=======
     * Header canon method.
>>>>>>> update grav cms
     *
     * @var string
     */
    protected $_headerCanon = 'simple';

    /**
<<<<<<< HEAD
     * Headers not being signed
     *
     * @var array
     */
    protected $_ignoredHeaders = array();

    /**
     * Signer identity
     *
     * @var unknown_type
=======
     * Headers not being signed.
     *
     * @var array
     */
    protected $_ignoredHeaders = array('return-path' => true);

    /**
     * Signer identity.
     *
     * @var string
>>>>>>> update grav cms
     */
    protected $_signerIdentity;

    /**
<<<<<<< HEAD
     * BodyLength
=======
     * BodyLength.
>>>>>>> update grav cms
     *
     * @var int
     */
    protected $_bodyLen = 0;

    /**
<<<<<<< HEAD
     * Maximum signedLen
=======
     * Maximum signedLen.
>>>>>>> update grav cms
     *
     * @var int
     */
    protected $_maxLen = PHP_INT_MAX;

    /**
<<<<<<< HEAD
     * Embbed bodyLen in signature
     *
     * @var boolean
=======
     * Embbed bodyLen in signature.
     *
     * @var bool
>>>>>>> update grav cms
     */
    protected $_showLen = false;

    /**
<<<<<<< HEAD
     * When the signature has been applied (true means time()), false means not embedded
=======
     * When the signature has been applied (true means time()), false means not embedded.
>>>>>>> update grav cms
     *
     * @var mixed
     */
    protected $_signatureTimestamp = true;

    /**
     * When will the signature expires false means not embedded, if sigTimestamp is auto
<<<<<<< HEAD
     * Expiration is relative, otherwhise it's absolute
=======
     * Expiration is relative, otherwhise it's absolute.
>>>>>>> update grav cms
     *
     * @var int
     */
    protected $_signatureExpiration = false;

    /**
     * Must we embed signed headers?
     *
<<<<<<< HEAD
     * @var boolean
=======
     * @var bool
>>>>>>> update grav cms
     */
    protected $_debugHeaders = false;

    // work variables
    /**
<<<<<<< HEAD
     * Headers used to generate hash
=======
     * Headers used to generate hash.
>>>>>>> update grav cms
     *
     * @var array
     */
    protected $_signedHeaders = array();

    /**
<<<<<<< HEAD
     * If debugHeaders is set store debugDatas here
=======
     * If debugHeaders is set store debugDatas here.
>>>>>>> update grav cms
     *
     * @var string
     */
    private $_debugHeadersData = '';

    /**
<<<<<<< HEAD
     * Stores the bodyHash
=======
     * Stores the bodyHash.
>>>>>>> update grav cms
     *
     * @var string
     */
    private $_bodyHash = '';

    /**
<<<<<<< HEAD
     * Stores the signature header
=======
     * Stores the signature header.
>>>>>>> update grav cms
     *
     * @var Swift_Mime_Headers_ParameterizedHeader
     */
    protected $_dkimHeader;

<<<<<<< HEAD
    /**
     * Hash Handler
     *
     * @var hash_ressource
     */
    private $_headerHashHandler;

=======
>>>>>>> update grav cms
    private $_bodyHashHandler;

    private $_headerHash;

    private $_headerCanonData = '';

    private $_bodyCanonEmptyCounter = 0;

    private $_bodyCanonIgnoreStart = 2;

    private $_bodyCanonSpace = false;

    private $_bodyCanonLastChar = null;

    private $_bodyCanonLine = '';

    private $_bound = array();

    /**
<<<<<<< HEAD
     * Constructor
=======
     * Constructor.
>>>>>>> update grav cms
     *
     * @param string $privateKey
     * @param string $domainName
     * @param string $selector
     */
    public function __construct($privateKey, $domainName, $selector)
    {
        $this->_privateKey = $privateKey;
        $this->_domainName = $domainName;
<<<<<<< HEAD
        $this->_signerIdentity = '@' . $domainName;
=======
        $this->_signerIdentity = '@'.$domainName;
>>>>>>> update grav cms
        $this->_selector = $selector;
    }

    /**
<<<<<<< HEAD
     * Instanciate DKIMSigner
     * 
     * @param string $privateKey
     * @param string $domainName
     * @param string $selector
     * @return Swift_Signers_DKIMSigner
     */
    public static function newInstance($privateKey, $domainName, $selector) 
    {
    	return new static($privateKey, $domainName, $selector);
    }
    
    
    /**
     * Reset the Signer
=======
     * Instanciate DKIMSigner.
     *
     * @param string $privateKey
     * @param string $domainName
     * @param string $selector
     *
     * @return Swift_Signers_DKIMSigner
     */
    public static function newInstance($privateKey, $domainName, $selector)
    {
        return new static($privateKey, $domainName, $selector);
    }

    /**
     * Reset the Signer.
     *
>>>>>>> update grav cms
     * @see Swift_Signer::reset()
     */
    public function reset()
    {
        $this->_headerHash = null;
        $this->_signedHeaders = array();
<<<<<<< HEAD
        $this->_headerHashHandler = null;
=======
>>>>>>> update grav cms
        $this->_bodyHash = null;
        $this->_bodyHashHandler = null;
        $this->_bodyCanonIgnoreStart = 2;
        $this->_bodyCanonEmptyCounter = 0;
<<<<<<< HEAD
        $this->_bodyCanonLastChar = NULL;
=======
        $this->_bodyCanonLastChar = null;
>>>>>>> update grav cms
        $this->_bodyCanonSpace = false;
    }

    /**
     * Writes $bytes to the end of the stream.
     *
     * Writing may not happen immediately if the stream chooses to buffer.  If
     * you want to write these bytes with immediate effect, call {@link commit()}
     * after calling write().
     *
     * This method returns the sequence ID of the write (i.e. 1 for first, 2 for
     * second, etc etc).
     *
     * @param string $bytes
<<<<<<< HEAD
     * @return int
     * @throws Swift_IoException
=======
     *
     * @throws Swift_IoException
     *
     * @return int
>>>>>>> update grav cms
     */
    public function write($bytes)
    {
        $this->_canonicalizeBody($bytes);
        foreach ($this->_bound as $is) {
            $is->write($bytes);
        }
    }

    /**
     * For any bytes that are currently buffered inside the stream, force them
     * off the buffer.
     *
     * @throws Swift_IoException
     */
    public function commit()
    {
        // Nothing to do
        return;
    }

    /**
     * Attach $is to this stream.
     * The stream acts as an observer, receiving all data that is written.
     * All {@link write()} and {@link flushBuffers()} operations will be mirrored.
     *
     * @param Swift_InputByteStream $is
     */
    public function bind(Swift_InputByteStream $is)
    {
        // Don't have to mirror anything
        $this->_bound[] = $is;

        return;
    }

    /**
     * Remove an already bound stream.
     * If $is is not bound, no errors will be raised.
     * If the stream currently has any buffered data it will be written to $is
     * before unbinding occurs.
     *
     * @param Swift_InputByteStream $is
     */
    public function unbind(Swift_InputByteStream $is)
    {
        // Don't have to mirror anything
        foreach ($this->_bound as $k => $stream) {
            if ($stream === $is) {
                unset($this->_bound[$k]);
<<<<<<< HEAD
=======

>>>>>>> update grav cms
                return;
            }
        }

        return;
    }

    /**
     * Flush the contents of the stream (empty it) and set the internal pointer
     * to the beginning.
     *
     * @throws Swift_IoException
     */
    public function flushBuffers()
    {
        $this->reset();
    }

    /**
<<<<<<< HEAD
     * Set hash_algorithm, must be one of rsa-sha256 | rsa-sha1 defaults to rsa-sha256
     *
     * @param string $hash
=======
     * Set hash_algorithm, must be one of rsa-sha256 | rsa-sha1 defaults to rsa-sha256.
     *
     * @param string $hash
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function setHashAlgorithm($hash)
    {
        // Unable to sign with rsa-sha256
        if ($hash == 'rsa-sha1') {
            $this->_hashAlgorithm = 'rsa-sha1';
        } else {
            $this->_hashAlgorithm = 'rsa-sha256';
        }

        return $this;
    }

    /**
<<<<<<< HEAD
     * Set the body canonicalization algorithm
     *
     * @param string $canon
=======
     * Set the body canonicalization algorithm.
     *
     * @param string $canon
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function setBodyCanon($canon)
    {
        if ($canon == 'relaxed') {
            $this->_bodyCanon = 'relaxed';
        } else {
            $this->_bodyCanon = 'simple';
        }

        return $this;
    }

    /**
<<<<<<< HEAD
     * Set the header canonicalization algorithm
     *
     * @param string $canon
=======
     * Set the header canonicalization algorithm.
     *
     * @param string $canon
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function setHeaderCanon($canon)
    {
        if ($canon == 'relaxed') {
            $this->_headerCanon = 'relaxed';
        } else {
            $this->_headerCanon = 'simple';
        }

        return $this;
    }

    /**
<<<<<<< HEAD
     * Set the signer identity
     *
     * @param string $identity
=======
     * Set the signer identity.
     *
     * @param string $identity
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function setSignerIdentity($identity)
    {
        $this->_signerIdentity = $identity;

        return $this;
    }

    /**
<<<<<<< HEAD
     * Set the length of the body to sign
     *
     * @param mixed $len (bool or int)
=======
     * Set the length of the body to sign.
     *
     * @param mixed $len (bool or int)
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function setBodySignedLen($len)
    {
        if ($len === true) {
            $this->_showLen = true;
            $this->_maxLen = PHP_INT_MAX;
        } elseif ($len === false) {
<<<<<<< HEAD
            $this->showLen = false;
=======
            $this->_showLen = false;
>>>>>>> update grav cms
            $this->_maxLen = PHP_INT_MAX;
        } else {
            $this->_showLen = true;
            $this->_maxLen = (int) $len;
        }

        return $this;
    }

    /**
<<<<<<< HEAD
     * Set the signature timestamp
     *
     * @param timestamp $time
=======
     * Set the signature timestamp.
     *
     * @param int $time A timestamp
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function setSignatureTimestamp($time)
    {
        $this->_signatureTimestamp = $time;

        return $this;
    }

    /**
<<<<<<< HEAD
     * Set the signature expiration timestamp
     *
     * @param timestamp $time
=======
     * Set the signature expiration timestamp.
     *
     * @param int $time A timestamp
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function setSignatureExpiration($time)
    {
        $this->_signatureExpiration = $time;

        return $this;
    }

    /**
<<<<<<< HEAD
     * Enable / disable the DebugHeaders
     *
     * @param boolean $debug
=======
     * Enable / disable the DebugHeaders.
     *
     * @param bool $debug
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function setDebugHeaders($debug)
    {
        $this->_debugHeaders = (bool) $debug;

        return $this;
    }

    /**
<<<<<<< HEAD
     * Start Body
     *
=======
     * Start Body.
>>>>>>> update grav cms
     */
    public function startBody()
    {
        // Init
        switch ($this->_hashAlgorithm) {
            case 'rsa-sha256' :
                $this->_bodyHashHandler = hash_init('sha256');
                break;
            case 'rsa-sha1' :
                $this->_bodyHashHandler = hash_init('sha1');
                break;
        }
        $this->_bodyCanonLine = '';
    }

    /**
<<<<<<< HEAD
     * End Body
     *
=======
     * End Body.
>>>>>>> update grav cms
     */
    public function endBody()
    {
        $this->_endOfBody();
    }

    /**
<<<<<<< HEAD
     * Returns the list of Headers Tampered by this plugin
=======
     * Returns the list of Headers Tampered by this plugin.
>>>>>>> update grav cms
     *
     * @return array
     */
    public function getAlteredHeaders()
    {
        if ($this->_debugHeaders) {
            return array('DKIM-Signature', 'X-DebugHash');
        } else {
            return array('DKIM-Signature');
        }
    }

    /**
<<<<<<< HEAD
     * Adds an ignored Header
     *
     * @param string $header_name
=======
     * Adds an ignored Header.
     *
     * @param string $header_name
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function ignoreHeader($header_name)
    {
        $this->_ignoredHeaders[strtolower($header_name)] = true;

        return $this;
    }

    /**
<<<<<<< HEAD
     * Set the headers to sign
     *
     * @param Swift_Mime_HeaderSet $headers
=======
     * Set the headers to sign.
     *
     * @param Swift_Mime_HeaderSet $headers
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function setHeaders(Swift_Mime_HeaderSet $headers)
    {
        $this->_headerCanonData = '';
        // Loop through Headers
        $listHeaders = $headers->listAll();
        foreach ($listHeaders as $hName) {
            // Check if we need to ignore Header
<<<<<<< HEAD
            if (! isset($this->_ignoredHeaders[strtolower($hName)])) {
=======
            if (!isset($this->_ignoredHeaders[strtolower($hName)])) {
>>>>>>> update grav cms
                if ($headers->has($hName)) {
                    $tmp = $headers->getAll($hName);
                    foreach ($tmp as $header) {
                        if ($header->getFieldBody() != '') {
                            $this->_addHeader($header->toString());
                            $this->_signedHeaders[] = $header->getFieldName();
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
<<<<<<< HEAD
     * Add the signature to the given Headers
     *
     * @param Swift_Mime_HeaderSet $headers
=======
     * Add the signature to the given Headers.
     *
     * @param Swift_Mime_HeaderSet $headers
     *
>>>>>>> update grav cms
     * @return Swift_Signers_DKIMSigner
     */
    public function addSignature(Swift_Mime_HeaderSet $headers)
    {
        // Prepare the DKIM-Signature
        $params = array('v' => '1', 'a' => $this->_hashAlgorithm, 'bh' => base64_encode($this->_bodyHash), 'd' => $this->_domainName, 'h' => implode(': ', $this->_signedHeaders), 'i' => $this->_signerIdentity, 's' => $this->_selector);
        if ($this->_bodyCanon != 'simple') {
<<<<<<< HEAD
            $params['c'] = $this->_headerCanon . '/' . $this->_bodyCanon;
=======
            $params['c'] = $this->_headerCanon.'/'.$this->_bodyCanon;
>>>>>>> update grav cms
        } elseif ($this->_headerCanon != 'simple') {
            $params['c'] = $this->_headerCanon;
        }
        if ($this->_showLen) {
            $params['l'] = $this->_bodyLen;
        }
        if ($this->_signatureTimestamp === true) {
            $params['t'] = time();
            if ($this->_signatureExpiration !== false) {
                $params['x'] = $params['t'] + $this->_signatureExpiration;
            }
        } else {
            if ($this->_signatureTimestamp !== false) {
                $params['t'] = $this->_signatureTimestamp;
            }
            if ($this->_signatureExpiration !== false) {
                $params['x'] = $this->_signatureExpiration;
            }
        }
        if ($this->_debugHeaders) {
            $params['z'] = implode('|', $this->_debugHeadersData);
        }
        $string = '';
        foreach ($params as $k => $v) {
<<<<<<< HEAD
            $string .= $k . '=' . $v . '; ';
=======
            $string .= $k.'='.$v.'; ';
>>>>>>> update grav cms
        }
        $string = trim($string);
        $headers->addTextHeader('DKIM-Signature', $string);
        // Add the last DKIM-Signature
        $tmp = $headers->getAll('DKIM-Signature');
        $this->_dkimHeader = end($tmp);
<<<<<<< HEAD
        $this->_addHeader(trim($this->_dkimHeader->toString()) . "\r\n b=", true);
=======
        $this->_addHeader(trim($this->_dkimHeader->toString())."\r\n b=", true);
>>>>>>> update grav cms
        $this->_endOfHeaders();
        if ($this->_debugHeaders) {
            $headers->addTextHeader('X-DebugHash', base64_encode($this->_headerHash));
        }
<<<<<<< HEAD
        $this->_dkimHeader->setValue($string . " b=" . trim(chunk_split(base64_encode($this->_getEncryptedHash()), 73, " ")));
=======
        $this->_dkimHeader->setValue($string.' b='.trim(chunk_split(base64_encode($this->_getEncryptedHash()), 73, ' ')));
>>>>>>> update grav cms

        return $this;
    }

    /* Private helpers */

    protected function _addHeader($header, $is_sig = false)
    {
        switch ($this->_headerCanon) {
            case 'relaxed' :
                // Prepare Header and cascade
                $exploded = explode(':', $header, 2);
                $name = strtolower(trim($exploded[0]));
<<<<<<< HEAD
                $value = str_replace("\r\n", "", $exploded[1]);
                $value = preg_replace("/[ \t][ \t]+/", " ", $value);
                $header = $name . ":" . trim($value) . ($is_sig ? '' : "\r\n");
=======
                $value = str_replace("\r\n", '', $exploded[1]);
                $value = preg_replace("/[ \t][ \t]+/", ' ', $value);
                $header = $name.':'.trim($value).($is_sig ? '' : "\r\n");
>>>>>>> update grav cms
            case 'simple' :
                // Nothing to do
        }
        $this->_addToHeaderHash($header);
    }

<<<<<<< HEAD
    protected function _endOfHeaders()
    {
        //$this->_headerHash=hash_final($this->_headerHashHandler, true);
=======
    /**
     * @deprecated This method is currently useless in this class but it must be
     *             kept for BC reasons due to its "protected" scope. This method
     *             might be overriden by custom client code.
     */
    protected function _endOfHeaders()
    {
>>>>>>> update grav cms
    }

    protected function _canonicalizeBody($string)
    {
        $len = strlen($string);
        $canon = '';
<<<<<<< HEAD
        $method = ($this->_bodyCanon == "relaxed");
=======
        $method = ($this->_bodyCanon == 'relaxed');
>>>>>>> update grav cms
        for ($i = 0; $i < $len; ++$i) {
            if ($this->_bodyCanonIgnoreStart > 0) {
                --$this->_bodyCanonIgnoreStart;
                continue;
            }
            switch ($string[$i]) {
                case "\r" :
                    $this->_bodyCanonLastChar = "\r";
                    break;
                case "\n" :
                    if ($this->_bodyCanonLastChar == "\r") {
                        if ($method) {
                            $this->_bodyCanonSpace = false;
                        }
                        if ($this->_bodyCanonLine == '') {
                            ++$this->_bodyCanonEmptyCounter;
                        } else {
                            $this->_bodyCanonLine = '';
                            $canon .= "\r\n";
                        }
                    } else {
                        // Wooops Error
                        // todo handle it but should never happen
                    }
                    break;
<<<<<<< HEAD
                case " " :
=======
                case ' ' :
>>>>>>> update grav cms
                case "\t" :
                    if ($method) {
                        $this->_bodyCanonSpace = true;
                        break;
                    }
                default :
                    if ($this->_bodyCanonEmptyCounter > 0) {
                        $canon .= str_repeat("\r\n", $this->_bodyCanonEmptyCounter);
                        $this->_bodyCanonEmptyCounter = 0;
                    }
                    if ($this->_bodyCanonSpace) {
                        $this->_bodyCanonLine .= ' ';
                        $canon .= ' ';
                        $this->_bodyCanonSpace = false;
                    }
                    $this->_bodyCanonLine .= $string[$i];
                    $canon .= $string[$i];
            }
        }
        $this->_addToBodyHash($canon);
    }

    protected function _endOfBody()
    {
        // Add trailing Line return if last line is non empty
        if (strlen($this->_bodyCanonLine) > 0) {
            $this->_addToBodyHash("\r\n");
        }
        $this->_bodyHash = hash_final($this->_bodyHashHandler, true);
    }

    private function _addToBodyHash($string)
    {
        $len = strlen($string);
        if ($len > ($new_len = ($this->_maxLen - $this->_bodyLen))) {
            $string = substr($string, 0, $new_len);
            $len = $new_len;
        }
        hash_update($this->_bodyHashHandler, $string);
        $this->_bodyLen += $len;
    }

    private function _addToHeaderHash($header)
    {
        if ($this->_debugHeaders) {
            $this->_debugHeadersData[] = trim($header);
        }
        $this->_headerCanonData .= $header;
    }

<<<<<<< HEAD
=======
    /**
     * @throws Swift_SwiftException
     *
     * @return string
     */
>>>>>>> update grav cms
    private function _getEncryptedHash()
    {
        $signature = '';
        switch ($this->_hashAlgorithm) {
            case 'rsa-sha1':
                $algorithm = OPENSSL_ALGO_SHA1;
                break;
            case 'rsa-sha256':
                $algorithm = OPENSSL_ALGO_SHA256;
                break;
        }
<<<<<<< HEAD
        $pkeyId=openssl_get_privatekey($this->_privateKey);
        if (!$pkeyId) {
            throw new Swift_SwiftException('Unable to load DKIM Private Key ['.openssl_error_string().']');
        }
        if (openssl_sign($this->_headerCanonData, $signature, $this->_privateKey, $algorithm)) {
=======
        $pkeyId = openssl_get_privatekey($this->_privateKey);
        if (!$pkeyId) {
            throw new Swift_SwiftException('Unable to load DKIM Private Key ['.openssl_error_string().']');
        }
        if (openssl_sign($this->_headerCanonData, $signature, $pkeyId, $algorithm)) {
>>>>>>> update grav cms
            return $signature;
        }
        throw new Swift_SwiftException('Unable to sign DKIM Hash ['.openssl_error_string().']');
    }
}
