<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2004-2009 Chris Corbyn
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
<<<<<<< HEAD
 * Header Signer Interface used to apply Header-Based Signature to a message
 *
 * @package    Swift
 * @subpackage Signatures
 * @author     Xavier De Cock <xdecock@gmail.com>
=======
 * Header Signer Interface used to apply Header-Based Signature to a message.
 *
 * @author Xavier De Cock <xdecock@gmail.com>
>>>>>>> update grav cms
 */
interface Swift_Signers_HeaderSigner extends Swift_Signer, Swift_InputByteStream
{
    /**
<<<<<<< HEAD
     * Exclude an header from the signed headers
=======
     * Exclude an header from the signed headers.
>>>>>>> update grav cms
     *
     * @param string $header_name
     *
     * @return Swift_Signers_HeaderSigner
     */
    public function ignoreHeader($header_name);

    /**
<<<<<<< HEAD
     * Prepare the Signer to get a new Body
=======
     * Prepare the Signer to get a new Body.
>>>>>>> update grav cms
     *
     * @return Swift_Signers_HeaderSigner
     */
    public function startBody();

    /**
<<<<<<< HEAD
     * Give the signal that the body has finished streaming
=======
     * Give the signal that the body has finished streaming.
>>>>>>> update grav cms
     *
     * @return Swift_Signers_HeaderSigner
     */
    public function endBody();

    /**
<<<<<<< HEAD
     * Give the headers already given
=======
     * Give the headers already given.
>>>>>>> update grav cms
     *
     * @param Swift_Mime_SimpleHeaderSet $headers
     *
     * @return Swift_Signers_HeaderSigner
     */
    public function setHeaders(Swift_Mime_HeaderSet $headers);

    /**
<<<<<<< HEAD
     * Add the header(s) to the headerSet
=======
     * Add the header(s) to the headerSet.
>>>>>>> update grav cms
     *
     * @param Swift_Mime_HeaderSet $headers
     *
     * @return Swift_Signers_HeaderSigner
     */
    public function addSignature(Swift_Mime_HeaderSet $headers);

    /**
<<<<<<< HEAD
     * Return the list of header a signer might tamper
=======
     * Return the list of header a signer might tamper.
>>>>>>> update grav cms
     *
     * @return array
     */
    public function getAlteredHeaders();
}
