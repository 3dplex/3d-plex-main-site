<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2009 Fabien Potencier <fabien.potencier@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for spools.
 *
<<<<<<< HEAD
 * @package Swift
 * @author  Fabien Potencier
=======
 * @author Fabien Potencier
>>>>>>> update grav cms
 */
interface Swift_Spool
{
    /**
     * Starts this Spool mechanism.
     */
    public function start();

    /**
     * Stops this Spool mechanism.
     */
    public function stop();

    /**
     * Tests if this Spool mechanism has started.
     *
<<<<<<< HEAD
     * @return boolean
=======
     * @return bool
>>>>>>> update grav cms
     */
    public function isStarted();

    /**
     * Queues a message.
     *
     * @param Swift_Mime_Message $message The message to store
     *
<<<<<<< HEAD
     * @return boolean Whether the operation has succeeded
=======
     * @return bool Whether the operation has succeeded
>>>>>>> update grav cms
     */
    public function queueMessage(Swift_Mime_Message $message);

    /**
     * Sends messages using the given transport instance.
     *
     * @param Swift_Transport $transport        A transport instance
     * @param string[]        $failedRecipients An array of failures by-reference
     *
<<<<<<< HEAD
     * @return integer The number of sent emails
=======
     * @return int The number of sent emails
>>>>>>> update grav cms
     */
    public function flushQueue(Swift_Transport $transport, &$failedRecipients = null);
}
