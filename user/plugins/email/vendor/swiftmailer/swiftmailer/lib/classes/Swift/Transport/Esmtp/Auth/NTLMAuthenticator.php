<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2004-2009 Chris Corbyn
 *
 * This authentication is for Exchange servers. We support version 1 & 2.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Handles NTLM authentication.
 *
<<<<<<< HEAD
 * @package    Swift
 * @subpackage Transport
 * @author     Ward Peeters <ward@coding-tech.com>
=======
 * @author Ward Peeters <ward@coding-tech.com>
>>>>>>> update grav cms
 */
class Swift_Transport_Esmtp_Auth_NTLMAuthenticator implements Swift_Transport_Esmtp_Authenticator
{
    const NTLMSIG = "NTLMSSP\x00";
<<<<<<< HEAD
    const DESCONST = "KGS!@#$%";
=======
    const DESCONST = 'KGS!@#$%';
>>>>>>> update grav cms

    /**
     * Get the name of the AUTH mechanism this Authenticator handles.
     *
     * @return string
     */
    public function getAuthKeyword()
    {
        return 'NTLM';
    }

    /**
     * Try to authenticate the user with $username and $password.
     *
     * @param Swift_Transport_SmtpAgent $agent
<<<<<<< HEAD
     * @param string $username
     * @param string $password
     *
     * @return boolean
     */
    public function authenticate(Swift_Transport_SmtpAgent $agent, $username, $password)
    {
        if (!function_exists('mcrypt_module_open')) {
            throw new LogicException('The mcrypt functions need to be enabled to use the NTLM authenticator.');
        }

        if (!function_exists('openssl_random_pseudo_bytes')) {
=======
     * @param string                    $username
     * @param string                    $password
     *
     * @return bool
     */
    public function authenticate(Swift_Transport_SmtpAgent $agent, $username, $password)
    {
        if (!function_exists('openssl_random_pseudo_bytes') || !function_exists('openssl_encrypt')) {
>>>>>>> update grav cms
            throw new LogicException('The OpenSSL extension must be enabled to use the NTLM authenticator.');
        }

        if (!function_exists('bcmul')) {
            throw new LogicException('The BCMatch functions must be enabled to use the NTLM authenticator.');
        }

        try {
            // execute AUTH command and filter out the code at the beginning
            // AUTH NTLM xxxx
            $response = base64_decode(substr(trim($this->sendMessage1($agent)), 4));

            // extra parameters for our unit cases
<<<<<<< HEAD
            $timestamp = func_num_args() > 3 ? func_get_arg(3) : $this->getCorrectTimestamp(bcmul(microtime(true), "1000"));
=======
            $timestamp = func_num_args() > 3 ? func_get_arg(3) : $this->getCorrectTimestamp(bcmul(microtime(true), '1000'));
>>>>>>> update grav cms
            $client = func_num_args() > 4 ? func_get_arg(4) : $this->getRandomBytes(8);

            // Message 3 response
            $this->sendMessage3($response, $username, $password, $timestamp, $client, $agent);

            return true;
        } catch (Swift_TransportException $e) {
            $agent->executeCommand("RSET\r\n", array(250));

            return false;
        }
    }

    protected function si2bin($si, $bits = 32)
    {
        $bin = null;
        if ($si >= -pow(2, $bits - 1) && ($si <= pow(2, $bits - 1))) {
            // positive or zero
            if ($si >= 0) {
                $bin = base_convert($si, 10, 2);
                // pad to $bits bit
                $bin_length = strlen($bin);
                if ($bin_length < $bits) {
<<<<<<< HEAD
                    $bin = str_repeat("0", $bits - $bin_length) . $bin;
                }
            } else { // negative
=======
                    $bin = str_repeat('0', $bits - $bin_length).$bin;
                }
            } else {
                // negative
>>>>>>> update grav cms
                $si = -$si - pow(2, $bits);
                $bin = base_convert($si, 10, 2);
                $bin_length = strlen($bin);
                if ($bin_length > $bits) {
<<<<<<< HEAD
                    $bin = str_repeat("1", $bits - $bin_length) . $bin;
=======
                    $bin = str_repeat('1', $bits - $bin_length).$bin;
>>>>>>> update grav cms
                }
            }
        }

        return $bin;
    }

    /**
<<<<<<< HEAD
     * Send our auth message and returns the response
     *
     * @param Swift_Transport_SmtpAgent $agent
=======
     * Send our auth message and returns the response.
     *
     * @param Swift_Transport_SmtpAgent $agent
     *
>>>>>>> update grav cms
     * @return string SMTP Response
     */
    protected function sendMessage1(Swift_Transport_SmtpAgent $agent)
    {
        $message = $this->createMessage1();

        return $agent->executeCommand(sprintf("AUTH %s %s\r\n", $this->getAuthKeyword(), base64_encode($message)), array(334));
    }

    /**
<<<<<<< HEAD
     * Fetch all details of our response (message 2)
     *
     * @param string $response
=======
     * Fetch all details of our response (message 2).
     *
     * @param string $response
     *
>>>>>>> update grav cms
     * @return array our response parsed
     */
    protected function parseMessage2($response)
    {
        $responseHex = bin2hex($response);
        $length = floor(hexdec(substr($responseHex, 28, 4)) / 256) * 2;
        $offset = floor(hexdec(substr($responseHex, 32, 4)) / 256) * 2;
        $challenge = $this->hex2bin(substr($responseHex, 48, 16));
        $context = $this->hex2bin(substr($responseHex, 64, 16));
        $targetInfoH = $this->hex2bin(substr($responseHex, 80, 16));
        $targetName = $this->hex2bin(substr($responseHex, $offset, $length));
        $offset = floor(hexdec(substr($responseHex, 88, 4)) / 256) * 2;
        $targetInfoBlock = substr($responseHex, $offset);
        list($domainName, $serverName, $DNSDomainName, $DNSServerName, $terminatorByte) = $this->readSubBlock($targetInfoBlock);

        return array(
            $challenge,
            $context,
            $targetInfoH,
            $targetName,
            $domainName,
            $serverName,
            $DNSDomainName,
            $DNSServerName,
            $this->hex2bin($targetInfoBlock),
<<<<<<< HEAD
            $terminatorByte
=======
            $terminatorByte,
>>>>>>> update grav cms
        );
    }

    /**
<<<<<<< HEAD
     * Read the blob information in from message2
     *
     * @param $block
=======
     * Read the blob information in from message2.
     *
     * @param $block
     *
>>>>>>> update grav cms
     * @return array
     */
    protected function readSubBlock($block)
    {
        // remove terminatorByte cause it's always the same
        $block = substr($block, 0, -8);

        $length = strlen($block);
        $offset = 0;
        $data = array();
        while ($offset < $length) {
            $blockLength = hexdec(substr(substr($block, $offset, 8), -4)) / 256;
            $offset += 8;
            $data[] = $this->hex2bin(substr($block, $offset, $blockLength * 2));
            $offset += $blockLength * 2;
        }

        if (count($data) == 3) {
            $data[] = $data[2];
            $data[2] = '';
        }

        $data[] = $this->createByte('00');

        return $data;
    }

    /**
<<<<<<< HEAD
     * Send our final message with all our data
     *
     * @param string $response Message 1 response (message 2)
     * @param string $username
     * @param string $password
     * @param string $timestamp
     * @param string $client
     * @param Swift_Transport_SmtpAgent $agent
     * @param bool $v2 Use version2 of the protocol
=======
     * Send our final message with all our data.
     *
     * @param string                    $response  Message 1 response (message 2)
     * @param string                    $username
     * @param string                    $password
     * @param string                    $timestamp
     * @param string                    $client
     * @param Swift_Transport_SmtpAgent $agent
     * @param bool                      $v2        Use version2 of the protocol
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function sendMessage3($response, $username, $password, $timestamp, $client, Swift_Transport_SmtpAgent $agent, $v2 = true)
    {
        list($domain, $username) = $this->getDomainAndUsername($username);
        //$challenge, $context, $targetInfoH, $targetName, $domainName, $workstation, $DNSDomainName, $DNSServerName, $blob, $ter
        list($challenge, , , , , $workstation, , , $blob) = $this->parseMessage2($response);

        if (!$v2) {
            // LMv1
            $lmResponse = $this->createLMPassword($password, $challenge);
            // NTLMv1
            $ntlmResponse = $this->createNTLMPassword($password, $challenge);
        } else {
            // LMv2
            $lmResponse = $this->createLMv2Password($password, $username, $domain, $challenge, $client);
            // NTLMv2
            $ntlmResponse = $this->createNTLMv2Hash($password, $username, $domain, $challenge, $blob, $timestamp, $client);
        }

        $message = $this->createMessage3($domain, $username, $workstation, $lmResponse, $ntlmResponse);

        return $agent->executeCommand(sprintf("%s\r\n", base64_encode($message)), array(235));
    }

    /**
<<<<<<< HEAD
     * Create our message 1
=======
     * Create our message 1.
>>>>>>> update grav cms
     *
     * @return string
     */
    protected function createMessage1()
    {
        return self::NTLMSIG
<<<<<<< HEAD
        . $this->createByte('01') // Message 1
        . $this->createByte('0702'); // Flags
    }

    /**
     * Create our message 3
=======
        .$this->createByte('01') // Message 1
.$this->createByte('0702'); // Flags
    }

    /**
     * Create our message 3.
>>>>>>> update grav cms
     *
     * @param string $domain
     * @param string $username
     * @param string $workstation
     * @param string $lmResponse
     * @param string $ntlmResponse
<<<<<<< HEAD
=======
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function createMessage3($domain, $username, $workstation, $lmResponse, $ntlmResponse)
    {
        // Create security buffers
        $domainSec = $this->createSecurityBuffer($domain, 64);
        $domainInfo = $this->readSecurityBuffer(bin2hex($domainSec));
        $userSec = $this->createSecurityBuffer($username, ($domainInfo[0] + $domainInfo[1]) / 2);
        $userInfo = $this->readSecurityBuffer(bin2hex($userSec));
        $workSec = $this->createSecurityBuffer($workstation, ($userInfo[0] + $userInfo[1]) / 2);
        $workInfo = $this->readSecurityBuffer(bin2hex($workSec));
        $lmSec = $this->createSecurityBuffer($lmResponse, ($workInfo[0] + $workInfo[1]) / 2, true);
        $lmInfo = $this->readSecurityBuffer(bin2hex($lmSec));
        $ntlmSec = $this->createSecurityBuffer($ntlmResponse, ($lmInfo[0] + $lmInfo[1]) / 2, true);

        return self::NTLMSIG
<<<<<<< HEAD
        . $this->createByte('03') // TYPE 3 message
        . $lmSec // LM response header
        . $ntlmSec // NTLM response header
        . $domainSec // Domain header
        . $userSec // User header
        . $workSec // Workstation header
        . $this->createByte("000000009a", 8) // session key header (empty)
        . $this->createByte('01020000') // FLAGS
        . $this->convertTo16bit($domain) // domain name
        . $this->convertTo16bit($username) // username
        . $this->convertTo16bit($workstation) // workstation
        . $lmResponse
        . $ntlmResponse;
    }

    /**
     * @param string $timestamp Epoch timestamp in microseconds
     * @param string $client Random bytes
     * @param string $targetInfo
=======
        .$this->createByte('03') // TYPE 3 message
.$lmSec // LM response header
.$ntlmSec // NTLM response header
.$domainSec // Domain header
.$userSec // User header
.$workSec // Workstation header
.$this->createByte('000000009a', 8) // session key header (empty)
.$this->createByte('01020000') // FLAGS
.$this->convertTo16bit($domain) // domain name
.$this->convertTo16bit($username) // username
.$this->convertTo16bit($workstation) // workstation
.$lmResponse
        .$ntlmResponse;
    }

    /**
     * @param string $timestamp  Epoch timestamp in microseconds
     * @param string $client     Random bytes
     * @param string $targetInfo
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function createBlob($timestamp, $client, $targetInfo)
    {
        return $this->createByte('0101')
<<<<<<< HEAD
        . $this->createByte('00')
        . $timestamp
        . $client
        . $this->createByte('00')
        . $targetInfo
        . $this->createByte('00');
    }

    /**
     * Get domain and username from our username
=======
        .$this->createByte('00')
        .$timestamp
        .$client
        .$this->createByte('00')
        .$targetInfo
        .$this->createByte('00');
    }

    /**
     * Get domain and username from our username.
>>>>>>> update grav cms
     *
     * @example DOMAIN\username
     *
     * @param string $name
<<<<<<< HEAD
=======
     *
>>>>>>> update grav cms
     * @return array
     */
    protected function getDomainAndUsername($name)
    {
        if (strpos($name, '\\') !== false) {
            return explode('\\', $name);
        }

        list($user, $domain) = explode('@', $name);

        return array($domain, $user);
    }

    /**
<<<<<<< HEAD
     * Create LMv1 response
     *
     * @param string $password
     * @param string $challenge
=======
     * Create LMv1 response.
     *
     * @param string $password
     * @param string $challenge
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function createLMPassword($password, $challenge)
    {
        // FIRST PART
        $password = $this->createByte(strtoupper($password), 14, false);
        list($key1, $key2) = str_split($password, 7);

        $desKey1 = $this->createDesKey($key1);
        $desKey2 = $this->createDesKey($key2);

<<<<<<< HEAD
        $constantDecrypt = $this->createByte($this->desEncrypt(self::DESCONST, $desKey1) . $this->desEncrypt(self::DESCONST, $desKey2), 21, false);
=======
        $constantDecrypt = $this->createByte($this->desEncrypt(self::DESCONST, $desKey1).$this->desEncrypt(self::DESCONST, $desKey2), 21, false);
>>>>>>> update grav cms

        // SECOND PART
        list($key1, $key2, $key3) = str_split($constantDecrypt, 7);

        $desKey1 = $this->createDesKey($key1);
        $desKey2 = $this->createDesKey($key2);
        $desKey3 = $this->createDesKey($key3);

<<<<<<< HEAD
        return $this->desEncrypt($challenge, $desKey1) . $this->desEncrypt($challenge, $desKey2) . $this->desEncrypt($challenge, $desKey3);
    }

    /**
     * Create NTLMv1 response
     *
     * @param string $password
     * @param string $challenge
=======
        return $this->desEncrypt($challenge, $desKey1).$this->desEncrypt($challenge, $desKey2).$this->desEncrypt($challenge, $desKey3);
    }

    /**
     * Create NTLMv1 response.
     *
     * @param string $password
     * @param string $challenge
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function createNTLMPassword($password, $challenge)
    {
        // FIRST PART
        $ntlmHash = $this->createByte($this->md4Encrypt($password), 21, false);
        list($key1, $key2, $key3) = str_split($ntlmHash, 7);

        $desKey1 = $this->createDesKey($key1);
        $desKey2 = $this->createDesKey($key2);
        $desKey3 = $this->createDesKey($key3);

<<<<<<< HEAD
        return $this->desEncrypt($challenge, $desKey1) . $this->desEncrypt($challenge, $desKey2) . $this->desEncrypt($challenge, $desKey3);
    }

    /**
     * Convert a normal timestamp to a tenth of a microtime epoch time
     *
     * @param string $time
=======
        return $this->desEncrypt($challenge, $desKey1).$this->desEncrypt($challenge, $desKey2).$this->desEncrypt($challenge, $desKey3);
    }

    /**
     * Convert a normal timestamp to a tenth of a microtime epoch time.
     *
     * @param string $time
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function getCorrectTimestamp($time)
    {
        // Get our timestamp (tricky!)
        bcscale(0);

        $time = number_format($time, 0, '.', ''); // save microtime to string
<<<<<<< HEAD
        $time = bcadd($time, "11644473600000"); // add epoch time
        $time = bcmul($time, 10000); // tenths of a microsecond.

        $binary = $this->si2bin($time, 64); // create 64 bit binary string
        $timestamp = "";
        for ($i = 0; $i < 8; $i++) {
=======
        $time = bcadd($time, '11644473600000'); // add epoch time
        $time = bcmul($time, 10000); // tenths of a microsecond.

        $binary = $this->si2bin($time, 64); // create 64 bit binary string
        $timestamp = '';
        for ($i = 0; $i < 8; ++$i) {
>>>>>>> update grav cms
            $timestamp .= chr(bindec(substr($binary, -(($i + 1) * 8), 8)));
        }

        return $timestamp;
    }

    /**
<<<<<<< HEAD
     * Create LMv2 response
=======
     * Create LMv2 response.
>>>>>>> update grav cms
     *
     * @param string $password
     * @param string $username
     * @param string $domain
     * @param string $challenge NTLM Challenge
<<<<<<< HEAD
     * @param string $client Random string
=======
     * @param string $client    Random string
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function createLMv2Password($password, $username, $domain, $challenge, $client)
    {
        $lmPass = '00'; // by default 00
        // if $password > 15 than we can't use this method
        if (strlen($password) <= 15) {
            $ntlmHash = $this->md4Encrypt($password);
<<<<<<< HEAD
            $ntml2Hash = $this->md5Encrypt($ntlmHash, $this->convertTo16bit(strtoupper($username) . $domain));

            $lmPass = bin2hex($this->md5Encrypt($ntml2Hash, $challenge . $client) . $client);
=======
            $ntml2Hash = $this->md5Encrypt($ntlmHash, $this->convertTo16bit(strtoupper($username).$domain));

            $lmPass = bin2hex($this->md5Encrypt($ntml2Hash, $challenge.$client).$client);
>>>>>>> update grav cms
        }

        return $this->createByte($lmPass, 24);
    }

    /**
<<<<<<< HEAD
     * Create NTLMv2 response
=======
     * Create NTLMv2 response.
>>>>>>> update grav cms
     *
     * @param string $password
     * @param string $username
     * @param string $domain
<<<<<<< HEAD
     * @param string $challenge Hex values
     * @param string $targetInfo Hex values
     * @param string $timestamp
     * @param string $client Random bytes
     * @return string
=======
     * @param string $challenge  Hex values
     * @param string $targetInfo Hex values
     * @param string $timestamp
     * @param string $client     Random bytes
     *
     * @return string
     *
>>>>>>> update grav cms
     * @see http://davenport.sourceforge.net/ntlm.html#theNtlmResponse
     */
    protected function createNTLMv2Hash($password, $username, $domain, $challenge, $targetInfo, $timestamp, $client)
    {
        $ntlmHash = $this->md4Encrypt($password);
<<<<<<< HEAD
        $ntml2Hash = $this->md5Encrypt($ntlmHash, $this->convertTo16bit(strtoupper($username) . $domain));
=======
        $ntml2Hash = $this->md5Encrypt($ntlmHash, $this->convertTo16bit(strtoupper($username).$domain));
>>>>>>> update grav cms

        // create blob
        $blob = $this->createBlob($timestamp, $client, $targetInfo);

<<<<<<< HEAD
        $ntlmv2Response = $this->md5Encrypt($ntml2Hash, $challenge . $blob);

        return $ntlmv2Response . $blob;
=======
        $ntlmv2Response = $this->md5Encrypt($ntml2Hash, $challenge.$blob);

        return $ntlmv2Response.$blob;
>>>>>>> update grav cms
    }

    protected function createDesKey($key)
    {
        $material = array(bin2hex($key[0]));
        $len = strlen($key);
<<<<<<< HEAD
        for ($i = 1; $i < $len; $i++) {
            list($high, $low) = str_split(bin2hex($key[$i]));
            $v = $this->castToByte(ord($key[$i - 1]) << (7 + 1 - $i) | $this->uRShift(hexdec(dechex(hexdec($high) & 0xf) . dechex(hexdec($low) & 0xf)), $i));
=======
        for ($i = 1; $i < $len; ++$i) {
            list($high, $low) = str_split(bin2hex($key[$i]));
            $v = $this->castToByte(ord($key[$i - 1]) << (7 + 1 - $i) | $this->uRShift(hexdec(dechex(hexdec($high) & 0xf).dechex(hexdec($low) & 0xf)), $i));
>>>>>>> update grav cms
            $material[] = str_pad(substr(dechex($v), -2), 2, '0', STR_PAD_LEFT); // cast to byte
        }
        $material[] = str_pad(substr(dechex($this->castToByte(ord($key[6]) << 1)), -2), 2, '0');

        // odd parity
        foreach ($material as $k => $v) {
            $b = $this->castToByte(hexdec($v));
            $needsParity = (($this->uRShift($b, 7) ^ $this->uRShift($b, 6) ^ $this->uRShift($b, 5)
                        ^ $this->uRShift($b, 4) ^ $this->uRShift($b, 3) ^ $this->uRShift($b, 2)
                        ^ $this->uRShift($b, 1)) & 0x01) == 0;

            list($high, $low) = str_split($v);
            if ($needsParity) {
<<<<<<< HEAD
                $material[$k] = dechex(hexdec($high) | 0x0) . dechex(hexdec($low) | 0x1);
            } else {
                $material[$k] = dechex(hexdec($high) & 0xf) . dechex(hexdec($low) & 0xe);
=======
                $material[$k] = dechex(hexdec($high) | 0x0).dechex(hexdec($low) | 0x1);
            } else {
                $material[$k] = dechex(hexdec($high) & 0xf).dechex(hexdec($low) & 0xe);
>>>>>>> update grav cms
            }
        }

        return $this->hex2bin(implode('', $material));
    }

    /** HELPER FUNCTIONS */
    /**
<<<<<<< HEAD
     * Create our security buffer depending on length and offset
     *
     * @param string $value Value we want to put in
     * @param int $offset start of value
     * @param bool $is16 Do we 16bit string or not?
=======
     * Create our security buffer depending on length and offset.
     *
     * @param string $value  Value we want to put in
     * @param int    $offset start of value
     * @param bool   $is16   Do we 16bit string or not?
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function createSecurityBuffer($value, $offset, $is16 = false)
    {
        $length = strlen(bin2hex($value));
        $length = $is16 ? $length / 2 : $length;
        $length = $this->createByte(str_pad(dechex($length), 2, '0', STR_PAD_LEFT), 2);

<<<<<<< HEAD
        return $length . $length . $this->createByte(dechex($offset), 4);
    }

    /**
     * Read our security buffer to fetch length and offset of our value
     *
     * @param string $value Securitybuffer in hex
=======
        return $length.$length.$this->createByte(dechex($offset), 4);
    }

    /**
     * Read our security buffer to fetch length and offset of our value.
     *
     * @param string $value Securitybuffer in hex
     *
>>>>>>> update grav cms
     * @return array array with length and offset
     */
    protected function readSecurityBuffer($value)
    {
        $length = floor(hexdec(substr($value, 0, 4)) / 256) * 2;
        $offset = floor(hexdec(substr($value, 8, 4)) / 256) * 2;

        return array($length, $offset);
    }

    /**
<<<<<<< HEAD
     * Cast to byte java equivalent to (byte)
     *
     * @param int $v
=======
     * Cast to byte java equivalent to (byte).
     *
     * @param int $v
     *
>>>>>>> update grav cms
     * @return int
     */
    protected function castToByte($v)
    {
        return (($v + 128) % 256) - 128;
    }

    /**
     * Java unsigned right bitwise
<<<<<<< HEAD
     * $a >>> $b
     *
     * @param int $a
     * @param int $b
=======
     * $a >>> $b.
     *
     * @param int $a
     * @param int $b
     *
>>>>>>> update grav cms
     * @return int
     */
    protected function uRShift($a, $b)
    {
        if ($b == 0) {
            return $a;
        }

        return ($a >> $b) & ~(1 << (8 * PHP_INT_SIZE - 1) >> ($b - 1));
    }

    /**
<<<<<<< HEAD
     * Right padding with 0 to certain length
     *
     * @param string $input
     * @param int $bytes Length of bytes
     * @param bool $isHex Did we provided hex value
=======
     * Right padding with 0 to certain length.
     *
     * @param string $input
     * @param int    $bytes Length of bytes
     * @param bool   $isHex Did we provided hex value
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function createByte($input, $bytes = 4, $isHex = true)
    {
        if ($isHex) {
            $byte = $this->hex2bin(str_pad($input, $bytes * 2, '00'));
        } else {
            $byte = str_pad($input, $bytes, "\x00");
        }

        return $byte;
    }

    /**
<<<<<<< HEAD
     * Create random bytes
     *
     * @param $length
=======
     * Create random bytes.
     *
     * @param $length
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function getRandomBytes($length)
    {
        $bytes = openssl_random_pseudo_bytes($length, $strong);

        if (false !== $bytes && true === $strong) {
            return $bytes;
        }

        throw new RuntimeException('OpenSSL did not produce a secure random number.');
    }

    /** ENCRYPTION ALGORITHMS */
    /**
<<<<<<< HEAD
     * DES Encryption
     *
     * @param string $value
     * @param string $key
=======
     * DES Encryption.
     *
     * @param string $value An 8-byte string
     * @param string $key
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function desEncrypt($value, $key)
    {
<<<<<<< HEAD
        $cipher = mcrypt_module_open(MCRYPT_DES, '', 'ecb', '');
        mcrypt_generic_init($cipher, $key, mcrypt_create_iv(mcrypt_enc_get_iv_size($cipher), MCRYPT_DEV_RANDOM));

        return mcrypt_generic($cipher, $value);
    }

    /**
     * MD5 Encryption
     *
     * @param string $key Encryption key
     * @param string $msg Message to encrypt
=======
        // 1 == OPENSSL_RAW_DATA - but constant is only available as of PHP 5.4.
        return substr(openssl_encrypt($value, 'DES-ECB', $key, 1), 0, 8);
    }

    /**
     * MD5 Encryption.
     *
     * @param string $key Encryption key
     * @param string $msg Message to encrypt
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function md5Encrypt($key, $msg)
    {
        $blocksize = 64;
        if (strlen($key) > $blocksize) {
            $key = pack('H*', md5($key));
        }

        $key = str_pad($key, $blocksize, "\0");
        $ipadk = $key ^ str_repeat("\x36", $blocksize);
        $opadk = $key ^ str_repeat("\x5c", $blocksize);

<<<<<<< HEAD
        return pack('H*', md5($opadk . pack('H*', md5($ipadk . $msg))));
    }

    /**
     * MD4 Encryption
     *
     * @param string $input
     * @return string
=======
        return pack('H*', md5($opadk.pack('H*', md5($ipadk.$msg))));
    }

    /**
     * MD4 Encryption.
     *
     * @param string $input
     *
     * @return string
     *
>>>>>>> update grav cms
     * @see http://php.net/manual/en/ref.hash.php
     */
    protected function md4Encrypt($input)
    {
        $input = $this->convertTo16bit($input);

        return function_exists('hash') ? $this->hex2bin(hash('md4', $input)) : mhash(MHASH_MD4, $input);
    }

    /**
<<<<<<< HEAD
     * Convert UTF-8 to UTF-16
     *
     * @param string $input
=======
     * Convert UTF-8 to UTF-16.
     *
     * @param string $input
     *
>>>>>>> update grav cms
     * @return string
     */
    protected function convertTo16bit($input)
    {
        return iconv('UTF-8', 'UTF-16LE', $input);
    }

    /**
<<<<<<< HEAD
     * Hex2bin replacement for < PHP 5.4
     * @param string $hex
=======
     * Hex2bin replacement for < PHP 5.4.
     *
     * @param string $hex
     *
>>>>>>> update grav cms
     * @return string Binary
     */
    protected function hex2bin($hex)
    {
        if (function_exists('hex2bin')) {
            return hex2bin($hex);
        } else {
            return pack('H*', $hex);
        }
    }

    /**
     * @param string $message
     */
    protected function debug($message)
    {
        $message = bin2hex($message);
        $messageId = substr($message, 16, 8);
<<<<<<< HEAD
        echo substr($message, 0, 16) . " NTLMSSP Signature<br />\n";
        echo $messageId . " Type Indicator<br />\n";

        if ($messageId == "02000000") {
=======
        echo substr($message, 0, 16)." NTLMSSP Signature<br />\n";
        echo $messageId." Type Indicator<br />\n";

        if ($messageId == '02000000') {
>>>>>>> update grav cms
            $map = array(
                'Challenge',
                'Context',
                'Target Information Security Buffer',
                'Target Name Data',
                'NetBIOS Domain Name',
                'NetBIOS Server Name',
                'DNS Domain Name',
                'DNS Server Name',
                'BLOB',
                'Target Information Terminator',
            );

            $data = $this->parseMessage2($this->hex2bin($message));

            foreach ($map as $key => $value) {
<<<<<<< HEAD
                echo bin2hex($data[$key]) . ' - ' . $data[$key] . ' ||| ' . $value . "<br />\n";
            }
        } elseif ($messageId == "03000000") {
=======
                echo bin2hex($data[$key]).' - '.$data[$key].' ||| '.$value."<br />\n";
            }
        } elseif ($messageId == '03000000') {
>>>>>>> update grav cms
            $i = 0;
            $data[$i++] = substr($message, 24, 16);
            list($lmLength, $lmOffset) = $this->readSecurityBuffer($data[$i - 1]);

            $data[$i++] = substr($message, 40, 16);
            list($ntmlLength, $ntmlOffset) = $this->readSecurityBuffer($data[$i - 1]);

            $data[$i++] = substr($message, 56, 16);
            list($targetLength, $targetOffset) = $this->readSecurityBuffer($data[$i - 1]);

            $data[$i++] = substr($message, 72, 16);
            list($userLength, $userOffset) = $this->readSecurityBuffer($data[$i - 1]);

            $data[$i++] = substr($message, 88, 16);
            list($workLength, $workOffset) = $this->readSecurityBuffer($data[$i - 1]);

            $data[$i++] = substr($message, 104, 16);
            $data[$i++] = substr($message, 120, 8);
            $data[$i++] = substr($message, $targetOffset, $targetLength);
            $data[$i++] = substr($message, $userOffset, $userLength);
            $data[$i++] = substr($message, $workOffset, $workLength);
            $data[$i++] = substr($message, $lmOffset, $lmLength);
            $data[$i] = substr($message, $ntmlOffset, $ntmlLength);

            $map = array(
                'LM Response Security Buffer',
                'NTLM Response Security Buffer',
                'Target Name Security Buffer',
                'User Name Security Buffer',
                'Workstation Name Security Buffer',
                'Session Key Security Buffer',
                'Flags',
                'Target Name Data',
                'User Name Data',
                'Workstation Name Data',
                'LM Response Data',
                'NTLM Response Data',
            );

            foreach ($map as $key => $value) {
<<<<<<< HEAD
                echo $data[$key] . ' - ' . $this->hex2bin($data[$key]) . ' ||| ' . $value . "<br />\n";
            }
        }

        echo "<br /><br />";
=======
                echo $data[$key].' - '.$this->hex2bin($data[$key]).' ||| '.$value."<br />\n";
            }
        }

        echo '<br /><br />';
>>>>>>> update grav cms
    }
}
