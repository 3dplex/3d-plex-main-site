<?php

/*
 * This file is part of SwiftMailer.
 * (c) 2004-2009 Chris Corbyn
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Handles PLAIN authentication.
 *
<<<<<<< HEAD
 * @package    Swift
 * @subpackage Transport
 * @author     Chris Corbyn
=======
 * @author Chris Corbyn
>>>>>>> update grav cms
 */
class Swift_Transport_Esmtp_Auth_PlainAuthenticator implements Swift_Transport_Esmtp_Authenticator
{
    /**
     * Get the name of the AUTH mechanism this Authenticator handles.
     *
     * @return string
     */
    public function getAuthKeyword()
    {
        return 'PLAIN';
    }

    /**
     * Try to authenticate the user with $username and $password.
     *
     * @param Swift_Transport_SmtpAgent $agent
     * @param string                    $username
     * @param string                    $password
     *
<<<<<<< HEAD
     * @return boolean
=======
     * @return bool
>>>>>>> update grav cms
     */
    public function authenticate(Swift_Transport_SmtpAgent $agent, $username, $password)
    {
        try {
<<<<<<< HEAD
            $message = base64_encode($username . chr(0) . $username . chr(0) . $password);
=======
            $message = base64_encode($username.chr(0).$username.chr(0).$password);
>>>>>>> update grav cms
            $agent->executeCommand(sprintf("AUTH PLAIN %s\r\n", $message), array(235));

            return true;
        } catch (Swift_TransportException $e) {
            $agent->executeCommand("RSET\r\n", array(250));

            return false;
        }
    }
}
