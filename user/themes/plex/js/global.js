jQuery(function($) {'use strict',
	//Preloader
	$(window).load(function(){
		$('.preloader').fadeOut('slow',function(){$(this).remove();});
	});
	//Parallax Scrolling
    if (!is.mobile()) {
        $(window).bind('load', function () {
            parallaxInit();
        });
        function parallaxInit() {

			$("#promo-one").parallax("50%", 0.3);
			$("#promo-two").parallax("50%", 0.3);
			$("#testimonial").parallax("50%", 0.3);
			$("#contact-us").parallax("50%", 0.3);

        }
        parallaxInit();
    }


	// Navigation Scroll
	$(window).on('scroll', function(){
		if( $(window).scrollTop()>600 ){
			$('#navigation .navbar').addClass('navbar-fixed-top');
		} else {
			$('#navigation .navbar').removeClass('navbar-fixed-top');
		}
	});

	$(window).scroll(function(event) {
		Scroll();
	});

	$('.navbar-collapse ul li a').click(function() {
		$('html, body').animate({scrollTop: $(this.hash).offset().top -10}, 1000);
		return false;
	});

	// User define function
	function Scroll() {
		var contentTop      =   [];
		var contentBottom   =   [];
		var winTop      =   $(window).scrollTop();
		var rangeTop    =   200;
		var rangeBottom =   500;
		$('.navbar-collapse').find('.scroll a').each(function(){
			contentTop.push( $( $(this).attr('href') ).offset().top);
			contentBottom.push( $( $(this).attr('href') ).offset().top + $( $(this).attr('href') ).height() );
		})
		$.each( contentTop, function(i){
			if ( winTop > contentTop[i] - rangeTop ){
				$('.navbar-collapse li.scroll')
				.removeClass('active')
				.eq(i).addClass('active');
			}
		})

	};

//	// Slider Height
	var slideHeight = $(window).height();
	$('#home-intro .item').css('height',slideHeight);

	$(window).resize(function(){'use strict',
		$('#home-intro .item').css('height',slideHeight);
	});


	//smoothScroll
	smoothScroll.init();

	//morphext
	$("#js-rotating").Morphext({
		// The [in] animation type. Refer to Animate.css for a list of available animations.
		animation: "fadeIn",
		// An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
		separator: ",",
		// The delay between the changing of each phrase in milliseconds.
		speed: 2000,
		complete: function () {
			// Called after the entrance animation is executed.
		}
	});
    
    // carousel interval
    $('.carousel').carousel({
        interval: 1000 * 10
    });

	//wow
	new WOW().init();

	toastr.options.progressBar = true;
	$('#contact-form').submit(function(e) {
		e.preventDefault();

		$.ajax({
			url: 'app/subscribe.php',
			type: 'POST',
			data: $('#contact-form').serialize(),
			success: function(result, status, xhr) {
				if (result.code) {
					toastr.error(result.error);
				} else {
					$('#sub').addClass('animated fadeOutDown').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
						$(this).addClass('hide');
						$('#subbed').removeClass('hide').addClass('animated fadeInUp');
					});
				}
			},
			error: function(xhr, text, data) {
				toastr.error('An unknown error occurred processing your request. Please try again later.');
			}
		});
	});

});
